package clone.analyzer;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

import org.junit.Test;

import ast.Stmt;
import sb3.dsl.SimXml2AstParser;

public class TestHashFunction {

	@Test
	public void testASTNodeHashEquality() {
		SimXml2AstParser parser = new SimXml2AstParser();
		Stmt stmt = (Stmt) parser.parse("<stmt type='1'></stmt>");
		Stmt stmt2 = (Stmt) parser.parse("<stmt type='1'></stmt>");
		assertThat(stmt.hashCode(), not(equalTo(stmt2.hashCode())));
	}

}
