package clone.analyzer;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.google.common.collect.Multimap;

import ast.BlockSeq;
import ast.Program;
import ast.Sprite;
import clone.analyzer.seqgen.SubSeqGenerator;
import clone.data.StmtSeq;

public class TestSubSeqGenerator {

	@Test
	public void testGenerateAllPossibleSubSeq() {
		// sprite1 contains 1 script [(1,2,1,2)]
		// sprite2 contains 2 script [(1,2),(1,2)]
		String spriteStr1 = "<sprite><script><stmt type='1'></stmt><stmt type='2'></stmt>"
				+ "<stmt type='1'></stmt><stmt type='2'></stmt></script></sprite>";
		String spriteStr2 = "<sprite><script><stmt type='1'></stmt><stmt type='2'></stmt></script>"
				+ "<script><stmt type='1'></stmt><stmt type='2'></stmt></script></sprite>";
		Program program = SimpProgramHelper.programFromMultiSpriteStr(spriteStr1, spriteStr2);

		SubSeqGenerator<Program> generator = new SubSeqGenerator<>(SequenceGenStrategy.ALL_POSSIBLE_SEQ_IN_PROGRAM);
		Multimap<Program, List<StmtSeq>> result = generator.gen(program);
		assertThat(result.keySet().size(), equalTo(1));
		assertThat(result.get(program).size(), equalTo(1));

		SubSeqGenerator<Sprite> generator2 = new SubSeqGenerator<>(SequenceGenStrategy.GROUP_BY_SPRITE);
		Multimap<Sprite, List<StmtSeq>> result2 = generator2.gen(program);
		assertThat(result2.keySet().size(), equalTo(2));

		SubSeqGenerator<BlockSeq> generator3 = new SubSeqGenerator<>(SequenceGenStrategy.GROUP_BY_BLOCKSEQS);
		Multimap<BlockSeq, List<StmtSeq>> result3 = generator3.gen(program);
		assertThat(result3.keySet().size(), equalTo(3));
	}
	
}
