package clone.analyzer;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.Lists;

import ast.ASTNode;
import ast.AttrAccess;
import ast.Expr;
import ast.ExprStmt;
import ast.NumLiteral;
import ast.OperatorExpr;
import ast.Program;
import ast.Sprite;
import clone.analyzer.SerializationConfig;
import clone.analyzer.ASTNodeHasher;
import clone.analyzer.ASTNodeSerializer;
import clone.analyzer.SerializationConfig.Builder;
import clone.data.CExpr;
import clone.data.StmtSeq;
import sb3.dsl.SimXml2AstParser;
import sb3.parser.xml.Sb3XmlParser;

public class AstSerializationTest {
	private static Logger LOGGER = null;

	private SimXml2AstParser parser;
	private ASTNodeSerializer serializer;

	@BeforeClass
	public static void setLogger() throws MalformedURLException {
		LOGGER = LogManager.getLogger();
		Configurator.setRootLevel(Level.DEBUG);
	}

	@Before
	public void setup() {
		parser = new SimXml2AstParser();
		serializer = new ASTNodeSerializer();
	}

	@Test
	public void configBuilder() {
		SerializationConfig config = new SerializationConfig.Builder().skipField(false).build();
		assertThat(config.skipField(), equalTo(false));
	}

	private String stringOfSerializedNodes(List<ASTNode> serialized) {
		StringBuilder sb = new StringBuilder();
		for (ASTNode node : serialized) {
			sb.append(node.shortName() + " ");
		}
		return sb.toString();
	}

	@Test
	public void simpleSerializetest() {
		String inputXml = "<expr><num>1</num><expr><num>4</num></expr></expr>";
		ASTNode ast = (ASTNode) parser.parse(inputXml);
		List<ASTNode> serialized = serializer.serialize(ast);
		LOGGER.debug(stringOfSerializedNodes(serialized));
		assertThat(serialized.size(), equalTo(4));
	}

	@Test
	public void shouldSerializeAttrAccOfExprByDefault() {
		String inputXml = "<expr><attr>1</attr><num>1</num></expr>";
		Expr ast = (Expr) parser.parse(inputXml);
		SerializationConfig config = new SerializationConfig.Builder().skipField(false).build();
		List<ASTNode> serialized = new ASTNodeSerializer(config).serialize(ast);
		LOGGER.debug(stringOfSerializedNodes(serialized));
		assertThat(serialized.size(), equalTo(3));
		assertThat(serialized.get(1), instanceOf(AttrAccess.class));
		assertThat(new CExpr(ast).size(), equalTo(serialized.size()));
	}

	@Test
	public void CCExprHashOfLiteral() {
		CExpr el1 = new CExpr((Expr) parser.parse("<num>1</num>"));
		CExpr el2 = new CExpr((Expr) parser.parse("<num>1</num>"));
		CExpr el3 = new CExpr((Expr) parser.parse("<num>2</num>"));
		assertThat(el1.hash(), equalTo(el2.hash()));
		assertThat(el1.hash(), not(equalTo(el3.hash())));
	}

	private int exprHash(String exprStr) {
		return new CExpr((Expr) parser.parse(exprStr)).hash();
	}

	@Test
	public void CCExprHashOfOpExp() {
		assertThat(exprHash("<expr type='op1'><num>1</num></expr>"),
				equalTo(exprHash("<expr type='op1'><num>1</num></expr>")));
		assertThat(exprHash("<expr type='op1'><num>2</num></expr>"),
				not(equalTo(exprHash("<expr type='op1'><num>1</num></expr>"))));
		assertThat(exprHash("<expr type='op1'></expr>"), not(equalTo(exprHash("<expr type='op2'></expr>"))));
		assertThat(exprHash("<expr type='op1'><expr type='op2'></expr></expr>"),
				not(equalTo(exprHash("<expr type='op2'><expr type='op1'></expr></expr>"))));
		assertThat(exprHash("<expr type='op1'><expr type='op2'></expr></expr>"),
				equalTo(exprHash("<expr type='op1'><expr type='op2'></expr></expr>")));
		assertThat(exprHash("<expr type='op1'><num>1</num><expr type='op2'></expr></expr>"),
				equalTo(exprHash("<expr type='op1'><num>1</num><expr type='op2'></expr></expr>")));
		assertThat(exprHash("<expr type='op1'><num>1</num><expr type='op2'></expr></expr>"),
				not(equalTo(exprHash("<expr type='op1'><expr type='op2'></expr><num>1</num></expr>"))));
	}

	@Test
	public void testRootExpr() {
		ExprStmt stmt = SimpProgramHelper.exprStmtOf("<stmt><expr type='op1'></expr></stmt>");
		assertThat(stmt.getArg(0).rootExpr(), equalTo(stmt.getArg(0)));
		Program testProgram = SimpProgramHelper
				.fromScripts("<script><stmt><expr id='e1'><expr id='e2'></expr></expr></stmt></script>").getProgram();
		assertThat(testProgram.lookupOpExpr("abc").isPresent(), equalTo(false));
		assertThat(testProgram.lookupOpExpr("e1").isPresent(), equalTo(true));
		assertThat(testProgram.lookupOpExpr("e2").get().rootExpr(), equalTo(testProgram.lookupOpExpr("e1").get()));
	}

	@Test
	public void collectAllOpExpr() {
		assertThat(SimpProgramHelper.fromScripts("<script></script>").firstSprite().blockSeqColl().size(), equalTo(0));
		assertThat(SimpProgramHelper.fromScripts("<script><stmt></stmt></script>").firstSprite().blockSeqColl().size(),
				equalTo(1));
		assertThat(SimpProgramHelper.fromScripts("<script><stmt></stmt></script>", "<script><stmt></stmt></script>")
				.firstSprite().blockSeqColl().size(), equalTo(2));
		assertThat(SimpProgramHelper.fromScripts("<script><ifelse><then><stmt></stmt></then></ifelse></script>")
				.firstSprite().blockSeqColl().size(), equalTo(2));
		assertThat(SimpProgramHelper
				.fromScripts("<script><ifelse><then><stmt></stmt></then><else><stmt></stmt></else></ifelse></script>")
				.firstSprite().blockSeqColl().size(), equalTo(3));
	}

	@Test
	public void collectOpExpr() {
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(
				"<xml xmlns=\"http://www.w3.org/1999/xhtml\"><variables></variables><block type=\"motion_movesteps\" id=\"Og1`ce=_{nmHej4=gSSb\" x=\"135\" y=\"136\"><value name=\"STEPS\"><shadow type=\"math_number\" id=\"AtB0wP5o[K(eb8^{BciB\"><field name=\"NUM\">10</field></shadow></value><next><block type=\"control_repeat\" id=\"NI%@U):twlcq64(zAj|v\"><value name=\"TIMES\"><shadow type=\"math_whole_number\" id=\"iWTN}a2hJ%rYijJ!b|=%\"><field name=\"NUM\">10</field></shadow></value><statement name=\"SUBSTACK\"><block type=\"motion_movesteps\" id=\"91u8|BwI:T%Fj-#LSeqc\"><value name=\"STEPS\"><shadow type=\"math_number\" id=\"[m?snB7rBK+6T4rT1,F6\"><field name=\"NUM\">10</field></shadow></value></block></statement></block></next></block></xml>");
		assertThat(new SimpProgramHelper().fromSprite(sprite).firstSprite().blockSeqColl().size(), equalTo(2));
	}

	@Test
	public void exprHash2() {
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		OperatorExpr expr1 = (OperatorExpr) sb3Parser.parseBlock(
				"<block type=\"operator_random\" id=\"ugmb#DM=tUKV%0CHVQX=\" x=\"44\" y=\"104\"><value name=\"FROM\"><shadow type=\"math_number\" id=\"~S{pln[-yDLOtta3pvl)\"><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><shadow type=\"math_number\" id=\"]H@;1_xAQ/8of=s/S[Ql\"><field name=\"NUM\">10</field></shadow><block type=\"operator_add\" id=\"#gBe#@f3h6bL]pChMNQC\"><value name=\"NUM1\"><shadow type=\"math_number\" id=\"A@Hwa-Zxe*H$MDf(XhD,\"><field name=\"NUM\">2</field></shadow></value><value name=\"NUM2\"><shadow type=\"math_number\" id=\"Hf_)@Z{^+lBO@b_r4*;]\"><field name=\"NUM\">3</field></shadow></value></block></value></block>");
		OperatorExpr expr2 = (OperatorExpr) sb3Parser.parseBlock(
				"<block type=\"operator_random\" id=\":DET^_^}^2=Gx`!O?Oav\" x=\"71\" y=\"143\"><value name=\"FROM\"><shadow type=\"math_number\" id=\"8[:+(oyL_ENSF]=`T::k\"><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><shadow type=\"math_number\" id=\"x6`};e]aQpoSVksZ)h,8\"><field name=\"NUM\">10</field></shadow><block type=\"operator_add\" id=\"R0qLjWWMPM/fjzh8V0Ey\"><value name=\"NUM1\"><shadow type=\"math_number\" id=\"~@gTHYxXNbTie0*p6V$|\"><field name=\"NUM\">2</field></shadow></value><value name=\"NUM2\"><shadow type=\"math_number\" id=\"ywzcda=Thgqpe06k8R^j\"><field name=\"NUM\">3</field></shadow></value></block></value></block>");
		assertThat(new CExpr(expr1).hash(), equalTo(new CExpr(expr2).hash()));
	}

	@Test
	public void hashOfSameStmtWithDiffInput() {
		ExprStmt stmt1 = (ExprStmt) parser.parse("<stmt type='c1'><num>1</num><expr></expr></stmt>");
		ExprStmt stmt2 = (ExprStmt) parser.parse("<stmt type='c1'><num>2</num><expr></expr></stmt>");

		SerializationConfig config = new SerializationConfig.Builder().skipField(false).skipInput(true).build();
		List<ASTNode> serialized1 = new ASTNodeSerializer(config).serializeASTSeq(Lists.newArrayList(stmt1));
		LOGGER.debug(stringOfSerializedNodes(serialized1));
		List<ASTNode> serialized2 = new ASTNodeSerializer(config).serializeASTSeq(Lists.newArrayList(stmt2));
		LOGGER.debug(stringOfSerializedNodes(serialized2));

		assertThat(new ASTNodeHasher(false).hashOf(serialized1), equalTo(new ASTNodeHasher(false).hashOf(serialized2)));
		assertThat(new StmtSeq(Lists.newArrayList(stmt1), null).hash(),
				equalTo(new StmtSeq(Lists.newArrayList(stmt2), null).hash()));
	}

	@Test
	public void serializeConsiderOnlyRootExpr() {
		ExprStmt stmt1 = (ExprStmt) parser.parse("<stmt type='c1'><num>1</num><attr>3</attr></stmt>");
		ExprStmt stmt2 = (ExprStmt) parser.parse("<stmt type='c1'><num>2</num><expr></expr></stmt>");
		LOGGER.info(stmt1.toDevString());

		SerializationConfig config = new SerializationConfig.Builder().skipInput(false).build();
		List<ASTNode> serialized1 = new ASTNodeSerializer(config).serializeASTSeq(Lists.newArrayList(stmt1));
		LOGGER.debug(stringOfSerializedNodes(serialized1));

		// List<ASTNode> serialized2 = new
		// ASTNodeSerializer(config).serializeASTSeq(Lists.newArrayList(stmt2));
		// LOGGER.debug(stringOfSerializedNodes(serialized2));
		//
		// assertThat(new ASTNodeHasher().hashOf(serialized1), equalTo(new
		// ASTNodeHasher().hashOf(serialized2)));
		// assertThat(new CStmtSeq(Lists.newArrayList(stmt1), null).hash(),
		// equalTo(new CStmtSeq(Lists.newArrayList(stmt2), null).hash()));
	}

}
