package clone.analyzer;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.net.MalformedURLException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.BeforeClass;
import org.junit.Test;

import ast.BlockSeq;
import ast.ExprStmt;
import ast.Program;
import ast.Script;
import ast.Sprite;
import ast.Stage;
import ast.Stmt;
import clone.result.CloneSeqResult;
import sb3.dsl.SimXml2AstParser;
import sb3.parser.xml.Sb3XmlParser;

public class TestCloneStmtSeqAnalyzer {
	private static Logger LOGGER = null;

	@BeforeClass
	public static void setLogger() throws MalformedURLException {
		LOGGER = LogManager.getLogger();
		Configurator.setRootLevel(Level.OFF);
	}

	@Test
	public void testCloneInSeparateScriptOfSameSprite() {
		/*
		 * 1 sprite contains 2 scripts: [(1,2,3), (1,2,3)] expect 1 clone group size 3,
		 * with clone part of size 3 statements
		 */
		Program program = new SimpProgramHelper().fromSprite(
				"<sprite><script><stmt type='1'></stmt><stmt type='2'></stmt><stmt type='3'></stmt></script>"
						+ "<script><stmt type='1'></stmt><stmt type='2'></stmt><stmt type='3'></stmt></script></sprite>");

		CloneStmtSeqAnalyzer<BlockSeq> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_BLOCKSEQS);
		CloneSeqResult<BlockSeq> res1 = analyzer.analyze(program);
		assertThat(res1.numGroup(), equalTo(0));

		CloneStmtSeqAnalyzer<Sprite> analyzer2 = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_SPRITE);
		CloneSeqResult<Sprite> res2 = analyzer2.analyze(program);
		assertThat(res2.numGroup(), equalTo(1));
		assertThat(res2.group(0).size(), equalTo(2));
		assertThat(res2.group(0).getPart(0).size(), equalTo(3));
	}

	@Test
	public void testCloneInSeparateScriptOfDifferentSprites() {
		/*
		 * sprite1 [(1,2,3)] sprite2 [(1,2,3)]
		 */
		String sprite1Str = "<sprite><script><stmt type='1'></stmt><stmt type='2'></stmt><stmt type='3'></stmt></script></sprite>";
		String sprite2Str = "<sprite><script><stmt type='1'></stmt><stmt type='2'></stmt><stmt type='3'></stmt></script></sprite>";
		Program program = SimpProgramHelper.programFromMultiSpriteStr(sprite1Str, sprite2Str);

		CloneStmtSeqAnalyzer<BlockSeq> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_BLOCKSEQS);
		CloneSeqResult<BlockSeq> res1 = analyzer.analyze(program);
		assertThat(res1.numGroup(), equalTo(0));

		CloneStmtSeqAnalyzer<Sprite> analyzer2 = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_SPRITE);
		CloneSeqResult<Sprite> res2 = analyzer2.analyze(program);
		assertThat(res2.numGroup(), equalTo(0));

		CloneStmtSeqAnalyzer<Program> analyzer3 = new CloneStmtSeqAnalyzer<>(
				SequenceGenStrategy.ALL_POSSIBLE_SEQ_IN_PROGRAM);
		CloneSeqResult<Program> res3 = analyzer3.analyze(program);
		assertThat(res3.numGroup(), equalTo(1));
	}

	@Test
	public void testSameSeqDifferentInput() {
		/*
		 * one script: stmt1(1),stmt2,stmt3, stmt1(2),stmt2,stmt3
		 */
		CloneStmtSeqAnalyzer<BlockSeq> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_BLOCKSEQS);
		Program program = new SimpProgramHelper().fromSprite(
				"<sprite><script><stmt type='1'><expr><num>1</num></expr></stmt><stmt type='2'></stmt><stmt type='3'></stmt><stmt type='1'><num>2</num></stmt><stmt type='2'></stmt><stmt type='3'></stmt></script></sprite>");
		CloneSeqResult<BlockSeq> result = analyzer.analyze(program);

		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
		assertThat(result.firstGroup().firstPart().asNameSeq(), contains("1", "2", "3"));
	}

	@Test
	public void testSameScratchSeqDifferentInput2() {
		CloneStmtSeqAnalyzer<Sprite> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_SPRITE);
		String spriteStr = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables></variables><block type='motion_glideto' id='U0c0B%Dq7qOn5iX]@=cD' x='127' y='145'><value name='SECS'><shadow type='math_number' id='{Z`i!S?p^/%lq.`2CA2~'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='motion_glideto_menu' id='R.;(;YE,QHd4o+-[l8o]'><field name='TO'>_mouse_</field></shadow></value><next><block type='motion_movesteps' id='N6[d}~,R/,80v:9cTUe8'><value name='STEPS'><shadow type='math_number' id='R0TP{l$qn3#,k(k%{VHQ'><field name='NUM'>10</field></shadow><block type='operator_random' id='h.mo4tU#+ohk?TtXMpJV'><value name='FROM'><shadow type='math_number' id='Pd5ZT5/aaTVsY+#c~-s?'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='math_number' id='kpdS37m(z2n^()}y/JYs'><field name='NUM'>21</field></shadow></value></block></value><next><block type='motion_turnright' id='un#.c{!mvw?f{;(zk8DC'><value name='DEGREES'><shadow type='math_number' id='},vg$+#MvqWqnWTGfrG%'><field name='NUM'>15</field></shadow></value><next><block type='motion_turnleft' id='buZDYM7~U7h#T2#RM3l3'><value name='DEGREES'><shadow type='math_number' id='s@B(d6Qjh:gR^{J-*ewq'><field name='NUM'>24</field></shadow></value><next><block type='motion_changexby' id='[5wt85,%lw~xW)Z=^?vE'><value name='DX'><shadow type='math_number' id='^,A,y}ea/oxxR0z/Fn,$'><field name='NUM'>10</field></shadow></value></block></next></block></next></block></next></block></next></block><block type='motion_gotoxy' id='q]D+x{51*g|!f3P9FMF0' x='560' y='163'><value name='X'><shadow type='math_number' id='i:|+A4#E7(Buf^9.Sv3.'><field name='NUM'>0</field></shadow></value><value name='Y'><shadow type='math_number' id='ABJW$OeYF(!}BY^~K07}'><field name='NUM'>0</field></shadow></value><next><block type='motion_movesteps' id='UQuQl:$9Xl/,@OL%(GXy'><value name='STEPS'><shadow type='math_number' id='A51`b0Dvc/5dja:vKG(A'><field name='NUM'>10</field></shadow><block type='operator_random' id='hX5n{^(lp#8b#I!mH[$-'><value name='FROM'><shadow type='math_number' id='WC!r$8y8%zgmO8fMi]O:'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='math_number' id='5:Z?hKT2.85E5(`Q,B9S'><field name='NUM'>10</field></shadow></value></block></value><next><block type='motion_turnright' id='6@LPbkpf[tgaWFd@mR.p'><value name='DEGREES'><shadow type='math_number' id='_r=FOTsJ,_.q0#6},,sf'><field name='NUM'>15</field></shadow></value><next><block type='motion_turnleft' id='Ir=B6!0aGh2RM|ISKoHJ'><value name='DEGREES'><shadow type='math_number' id='8%`E:WVeZ6wy7],=DE)j'><field name='NUM'>15</field></shadow></value><next><block type='motion_changeyby' id='[13[vhze!-J3U`vlKoe3'><value name='DY'><shadow type='math_number' id='PjRHO3R`r2`:krGQT,(E'><field name='NUM'>10</field></shadow></value></block></next></block></next></block></next></block></next></block></xml>";
		Program program = new ScratchProgramHelper().fromSprite(spriteStr);
		CloneSeqResult<Sprite> result = analyzer.analyze(program);

		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
		assertThat(result.firstGroup().firstPart().asNameSeq(),
				contains("motion_movesteps", "motion_turnright", "motion_turnleft"));
	}

	@Test
	public void testSimpBlockNestedSeq() {
		CloneStmtSeqAnalyzer<Sprite> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_SPRITE);
		String spriteXml = "<sprite><script><ifelse><cond><str>true</str></cond><then><stmt type='c1'><num>1</num></stmt><stmt type='c2'></stmt><stmt type='c3'></stmt></then><else><stmt type='c1'><num>2</num></stmt><stmt type='c2'></stmt><stmt type='c3'></stmt></else></ifelse><stmt type='c1'><num>3</num></stmt><stmt type='c2'></stmt></script></sprite>";
		Program program = new SimpProgramHelper().fromSprite(spriteXml);
		CloneSeqResult<Sprite> result = analyzer.analyze(program);
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
		assertThat(result.firstGroup().firstPart().asNameSeq(), contains("c1", "c2", "c3"));
	}

	@Test
	public void testScratchCloneSeq() {
		String spriteXml = "<xml><block type='control_repeat' id='QH.an1%`BFPp_IE%*},s' x='262' y='228'><value name='TIMES'><shadow type='math_whole_number' id='RtUOiK8l1pgAX4k.F2%4'><field name='NUM'>10</field></shadow></value><statement name='SUBSTACK'><block type='motion_movesteps' id='Ki%nw`xv[+o|tB7eUn!2'><value name='STEPS'><shadow type='math_number' id='XmyMO.JYz_q)~_1Ntjm+'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='|@z-143Iao%5;x]3Y0s:'><value name='DEGREES'><shadow type='math_number' id='m.QLTg|SRBQg4ez;z?XM'><field name='NUM'>20</field></shadow></value><next><block type='motion_turnleft' id='2rO5,-sUG%/As5WSr7PR'><value name='DEGREES'><shadow xmlns='' id='I,:ZD23`~GeW/7:Mc.e0' type='math_number'><field name='NUM'>10</field></shadow><block type='operator_mathop' id='Npj~=3p@5-i-dx;Z^*V/'><field name='OPERATOR'>sqrt</field><value name='NUM'><shadow type='math_number' id='7bAF47t4y#QAFK#2uUVz'><field name='NUM'>9</field></shadow></value></block></value></block></next></block></next></block></statement></block><block type='control_forever' id='p*4^CbSpHq-9k.nX]No=' x='697' y='226'><statement name='SUBSTACK'><block type='motion_movesteps' id='*AMfGE@f@t2J}%-]mGFy'><value name='STEPS'><shadow xmlns='' id='x7=]7R(7{Us3W44d9{l/' type='math_number'><field name='NUM'>10</field></shadow><block type='operator_random' id='_De*lJ(vrNe+`5m}Q,XH'><value name='FROM'><shadow type='math_number' id='e92ZUV[-lZM~R!Py(|RT'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='math_number' id='c0~Z7GC;gXs|=q)UW{~Q'><field name='NUM'>10</field></shadow></value></block></value><next><block type='motion_turnright' id='9y[B?+|hQ{_]Bjx!Gyw^'><value name='DEGREES'><shadow type='math_number' id='w6~lcz_2_m8a,hF%~-)*'><field name='NUM'>20</field></shadow></value><next><block type='motion_turnleft' id='-Ud[XbNAByAN1N{7d2j4'><value name='DEGREES'><shadow type='math_number' id='NL)2l0#%faU7SdhqRaeM'><field name='NUM'>30</field></shadow></value></block></next></block></next></block></statement></block></xml>";
		Program program = new ScratchProgramHelper().fromSprite(spriteXml);
		CloneStmtSeqAnalyzer<Sprite> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_SPRITE);
		CloneSeqResult<Sprite> result = analyzer.analyze(program);

		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2)); // consider clones in each scriptable separately
		assertThat(result.firstGroup().firstPart().asNameSeq(),
				contains("motion_movesteps", "motion_turnright", "motion_turnleft"));

		List<Stmt> stmts = result.firstGroup().firstPart().get();
		List<String> blockIds = stmts.stream().map(stmt -> (ExprStmt) stmt).map(stmt -> stmt.getBlockId())
				.collect(Collectors.toList());
		System.out.println("A clone instance : " + blockIds);
	}

	@Test
	public void testBugs() {
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(
				"<xml><variables><variable type=\"\" id=\"`jEk@4|i[#Fk?(8x)AV.-my variable\">my variable</variable><variable type=\"\" id=\"Y})TGj3O`[./JoG:jwvt\">1234</variable><variable type=\"broadcast_msg\" id=\",,f=dY5{V!Tu:y9F_Tyi\">message1</variable></variables><block type=\"event_whenbroadcastreceived\" id=\"1%2t8A6-vE6YE;5c8QzV\" x=\"216\" y=\"-50\"><field name=\"BROADCAST_OPTION\" id=\",,f=dY5{V!Tu:y9F_Tyi\" variabletype=\"broadcast_msg\">message1</field><next><block type=\"motion_turnright\" id=\"M?{T{,6@P8:wyMb;`X*M\"><value name=\"DEGREES\"><shadow type=\"math_number\" id=\"7h!2%Hb6=X|WoOvwy4B0\"><field name=\"NUM\">15</field></shadow></value></block></next></block></xml>");
		Program program = new SimpProgramHelper().programOf(sprite);
		CloneStmtSeqAnalyzer<Sprite> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_SPRITE);
		CloneSeqResult<Sprite> res = analyzer.analyze(program);
		System.out.println(res);
	}

	@Test
	public void testShouldNotConsiderHatBlock() {
		CloneStmtSeqAnalyzer<BlockSeq> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_SPRITE);
		Program program = new SimpProgramHelper().fromSprite(
				"<sprite><script><stmt type='event_whenflagclicked'><expr><num>1</num></expr></stmt><stmt type='2'></stmt><stmt type='3'></stmt><stmt type='event_whenflagclicked'><num>2</num></stmt><stmt type='2'></stmt><stmt type='3'></stmt></script></sprite>");
		CloneSeqResult<BlockSeq> result = analyzer.analyze(program);
		LOGGER.debug(result);
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
		assertThat(result.firstGroup().firstPart().asNameSeq(), contains("2", "3"));
	}

	@Test
	public void configureToConsiderPerTarget() {
		String sprite1Xml = "<sprite><script><stmt type='1'></stmt><stmt type='2'></stmt><stmt type='3'></stmt></script></sprite>";
		String sprite2Xml = "<sprite><script><stmt type='1'></stmt><stmt type='2'></stmt><stmt type='3'></stmt></script></sprite>";
		String scriptXml = "<script><stmt type='1'></stmt><stmt type='2'></stmt><stmt type='3'></stmt></script>";
		SimXml2AstParser parser = new SimXml2AstParser();
		Sprite sprite1 = (Sprite) parser.parse(sprite1Xml);
		Sprite sprite2 = (Sprite) parser.parse(sprite2Xml);
		Script script = (Script) parser.parse(scriptXml);
		Program program = new Program();
		program.setStage((new Stage()));
		program.getStage().addScript(script);
		program.getStage().addSprite(sprite1);
		program.getStage().addSprite(sprite2);
		CloneStmtSeqAnalyzer<Sprite> analyzer = new CloneStmtSeqAnalyzer<Sprite>(SequenceGenStrategy.GROUP_BY_SPRITE);
		CloneSeqResult<Sprite> res = analyzer.analyze(program);
		System.out.println(res);
		// TODO assert
	}

	@Test
	public void smallerSeqShouldBeEliminated() {
		CloneStmtSeqAnalyzer<Sprite> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_SPRITE);
		String spriteXml = "<xml><variables></variables><block type='event_whenflagclicked' id='-Yf=?+n{/g8q{!_O9#ZQ' x='383' y='113'><next><block type='control_repeat' id='d!xEOztQm#Jy$hW:PLD3'><value name='TIMES'><shadow type='math_whole_number' id='Y)/_;/a5a-%GeT*dSqLg'><field name='NUM'>10</field></shadow></value><statement name='SUBSTACK'><block type='control_wait' id='5$Kp}YUnB:(KIE`Yd-Ff'><value name='DURATION'><shadow type='math_positive_number' id='SfxJCw:hAj)U7bA/G|9o'><field name='NUM'>1</field></shadow></value><next><block type='looks_nextcostume' id='i:F[=|0Ii-eJ[767M5Lp'></block></next></block></statement><next><block type='motion_movesteps' id='4nd#qm)+SaXuqsfxFuE!'><value name='STEPS'><shadow type='math_number' id='W_7:i~T/GZ,!PRD`bt_b'><field name='NUM'>10</field></shadow></value><next><block type='control_repeat' id='fgcfeq?BA@WvLWn87XMy'><value name='TIMES'><shadow type='math_whole_number' id='EH%tSL!7VLyu{-26tKh2'><field name='NUM'>10</field></shadow></value><statement name='SUBSTACK'><block type='control_wait' id='7!BQpC;hV,2Ccg8e-CE?'><value name='DURATION'><shadow type='math_positive_number' id='{o*5NkF;;ROq6DDFu=Jk'><field name='NUM'>1</field></shadow></value><next><block type='looks_nextcostume' id='QdB}xUc`(l/dbqIVB.Yh'></block></next></block></statement></block></next></block></next></block></next></block></xml>";
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(spriteXml);
		Program program = new SimpProgramHelper().programOf(sprite);
		CloneSeqResult<Sprite> result = analyzer.analyze(program);

		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
	}

	@Test
	public void fixBugSmallerSeqShouldBeEliminated2() {
		CloneStmtSeqAnalyzer<Sprite> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_SPRITE);
		String spriteXml = "<xml><variables></variables><block type='control_forever' id=':;~#zZ^3?5ZH4KDXhG(c' x='165' y='156'><statement name='SUBSTACK'><block type='motion_movesteps' id='h*mLC+_)xIJhB~#1Vm2E'><value name='STEPS'><shadow type='math_number' id='#6H!++_t)JKRf,iPLK`$'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='_FxzLC7?55p4H;E%/wY{'><value name='DEGREES'><shadow type='math_number' id='Z@xH5c2QPy*gWa*34iQU'><field name='NUM'>15</field></shadow></value><next><block type='control_repeat' id='qB$(L`JhthgFtk7tQzmV'><value name='TIMES'><shadow type='math_whole_number' id=';1a:BAP6?DUv6)+rLI28'><field name='NUM'>10</field></shadow></value><statement name='SUBSTACK'><block type='motion_movesteps' id=']hn/x_iT`rb^ES[RC=Ef'><value name='STEPS'><shadow type='math_number' id='bivC.e0:n-pKm,}Cel7!'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='tt.gjWoeL~[lahkdZAK{'><value name='DEGREES'><shadow type='math_number' id=',Cm[u(6nxmdti(J$t_Ot'><field name='NUM'>15</field></shadow></value></block></next></block></statement><next><block type='control_forever' id='oLj0^_DDh!71+s$kn(y('><statement name='SUBSTACK'><block type='control_repeat' id='fHS@2I(Qi$4nCFRYXv9['><value name='TIMES'><shadow type='math_whole_number' id='off@zn/R:UM5lRMvFDEl'><field name='NUM'>10</field></shadow></value><statement name='SUBSTACK'><block type='motion_movesteps' id='_9BY%R5:.Q(%vaq=9D:2'><value name='STEPS'><shadow type='math_number' id='JOJD+c$Q3[0U`Ix0D=lh'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='y2t!/l(G}`au7wdy2Vli'><value name='DEGREES'><shadow type='math_number' id='?XM]Qwd^62D^7_t#JuJS'><field name='NUM'>15</field></shadow></value></block></next></block></statement></block></statement></block></next></block></next></block></next></block></statement></block></xml>";
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(spriteXml);
		Program program = new SimpProgramHelper().programOf(sprite);
		CloneSeqResult<Sprite> result = analyzer.analyze(program);
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
	}

	@Test
	public void shouldConsiderStmtInProcedure() {
		CloneStmtSeqAnalyzer<BlockSeq> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_BLOCKSEQS);
		// Program program = new SimpProgramHelper().fromSprite(
		// "<sprite><procedure proccode='M'><params><param id='p1'/></params><body><stmt
		// type='1'><expr><num>1</num></expr></stmt><stmt type='2'></stmt><stmt
		// type='3'></stmt><stmt type='1'><num>2</num></stmt><stmt type='2'></stmt><stmt
		// type='3'></stmt></body></procedure></sprite>");

		String spriteXml = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables></variables><block type='procedures_definition' id='Ht' x='175' y='333'><statement name='custom_block'><shadow type='procedures_prototype' id='1ZgHR'><mutation proccode='DoSomething1' argumentids='[]' argumentnames='[]' argumentdefaults='[]' warp='false'></mutation></shadow></statement><next><block type='motion_movesteps' id='ew'><value name='STEPS'><shadow type='math_number' id='Hi'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='XN'><value name='DEGREES'><shadow type='math_number' id='cO'><field name='NUM'>15</field></shadow></value><next><block type='motion_movesteps' id='bX'><value name='STEPS'><shadow type='math_number' id='S1'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='lq'><value name='DEGREES'><shadow type='math_number' id='8k'><field name='NUM'>15</field></shadow></value></block></next></block></next></block></next></block></next></block></xml>";
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(spriteXml);
		Program program = new SimpProgramHelper().programOf(sprite);

		CloneSeqResult<BlockSeq> result = analyzer.analyze(program);

		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
		assertThat(result.firstGroup().firstPart().asNameSeq(), contains("motion_movesteps", "motion_turnright"));
		System.out.println(result.firstGroup());
	}

	@Test
	public void shouldIgnoreConditionalSubtree() {
		CloneStmtSeqAnalyzer<BlockSeq> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_SPRITE);
		String spriteXml = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables></variables><block type='event_whenflagclicked' id='WTkomJRw!r:#xuKpN4Vb' x='321' y='163'><next><block type='control_if' id=':g;Q7-}(_]uPbIEEmgV5'><value name='CONDITION'><block type='sensing_touchingcolor' id='od(3u~xfbYVx/WdR:LXO'><value name='COLOR'><shadow type='colour_picker' id='1m?2B%P**Zdl=gM1m0zZ'><field name='COLOUR'>#ae68f8</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id='fnRY5c6FE}RHuoZ;b^+O'><value name='STEPS'><shadow type='math_number' id='VWcXtZ=_(HWlpni1fUBb'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='rO**uDrGjE!/HVAH.K=b'><value name='DEGREES'><shadow type='math_number' id='$dT/C[u-gNbdjLRiDpzn'><field name='NUM'>15</field></shadow></value></block></next></block></statement><next><block type='control_if' id=')6g6/=HBx8Vhk{.pNG)8'><value name='CONDITION'><block type='sensing_touchingcolor' id='v0kI:1?$oV]coX:7#*au'><value name='COLOR'><shadow type='colour_picker' id='{11-pV|PW2-s?TdAr^4J'><field name='COLOUR'>#f86666</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id='JRE(?(SG=4*9vUXCyDp8'><value name='STEPS'><shadow type='math_number' id='w1_H$[0bZG.doen?+Z}t'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='VU:T0uSC}sPYd)Mfw,2N'><value name='DEGREES'><shadow type='math_number' id='_FfqsKQyn!a[*w3U9m%P'><field name='NUM'>15</field></shadow></value></block></next></block></statement></block></next></block></next></block></xml>";
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(spriteXml);
		Program program = new SimpProgramHelper().programOf(sprite);

		CloneSeqResult<BlockSeq> result = analyzer.analyze(program);
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().firstPart().asNameSeq(), contains("motion_movesteps", "motion_turnright"));
	}

	@Test
	public void shouldDistinguishDestOfAssignStmt() {
		CloneStmtSeqAnalyzer<BlockSeq> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_SPRITE);
		String spriteXml = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables><variable type='' id='$f]ahz.x#)x%]Wm6m?iK' islocal='false' iscloud='false'>a</variable><variable type='' id='C{QuEE+:R_,BO=}E-,Bc' islocal='false' iscloud='false'>b</variable></variables><block type='motion_movesteps' id='sVF,==m2_#DNFPV!=MeO' x='424' y='151'><value name='STEPS'><shadow type='math_number' id=']NRWMk8|4U/+pZIXRp9Q'><field name='NUM'>10</field></shadow></value><next><block type='data_setvariableto' id='D+,48NuMz~9y~ku`$k$E'><field name='VARIABLE' id='$f]ahz.x#)x%]Wm6m?iK' variabletype=''>a</field><value name='VALUE'><shadow type='text' id='jg%f#viZ!L]`zUrWY.bD'><field name='TEXT'>0</field></shadow></value><next><block type='motion_movesteps' id='5qq6p_x4h$*hj}~y^U_x'><value name='STEPS'><shadow type='math_number' id='_.cR=z$h^LB1BFLz$N^i'><field name='NUM'>10</field></shadow></value><next><block type='data_setvariableto' id='/^@~Tu4zEBtqJO[sBt=_'><field name='VARIABLE' id='C{QuEE+:R_,BO=}E-,Bc' variabletype=''>b</field><value name='VALUE'><shadow type='text' id='t.bps2e:n{|2}HmhW2fw'><field name='TEXT'>0</field></shadow></value></block></next></block></next></block></next></block></xml>";
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(spriteXml);
		Program program = new SimpProgramHelper().programOf(sprite);

		CloneSeqResult<BlockSeq> result = analyzer.analyze(program);
		assertThat(result.numGroup(), equalTo(0));
	}
}
