package clone.analyzer;

import ast.Expr;
import ast.ExprStmt;
import ast.Program;
import ast.Sprite;
import ast.Stage;
import sb3.dsl.SimXml2AstParser;

public class SimpProgramHelper implements AbstractProgramHelper {
	private Program program;

	public SimpProgramHelper(Program program) {
		this.program = program;
	}

	public SimpProgramHelper() {
	}

	public Program getProgram() {
		return program;
	}

	public Expr exprOf(String exprStr) {
		return (Expr) new SimXml2AstParser().parse(exprStr);
	}

	public static ExprStmt exprStmtOf(String str) {
		return (ExprStmt) new SimXml2AstParser().parse(str);
	}

	public static SimpProgramHelper fromScripts(String... scripts) {
		String simpleProgramStr = "<program><sprite>%s</sprite></program>";
		StringBuilder sb = new StringBuilder();
		for (String script : scripts) {
			sb.append(script);
		}
		Program program = (Program) new SimXml2AstParser().parse(String.format(simpleProgramStr, sb.toString()));
		return new SimpProgramHelper(program);
	}

	public static Program programFromMultiSpriteStr(String... sprites) {
		String simpleProgramStr = "<program>%s</program>";
		StringBuilder sb = new StringBuilder();
		for (String sprite : sprites) {
			sb.append(sprite);
		}
		Program program = (Program) new SimXml2AstParser().parse(String.format(simpleProgramStr, sb.toString()));
		return program;
	}

	public Sprite firstSprite() {
		return program.getStage().getSprite(0);
	}

	public Program fromSprite(String xmlStr) {
		SimXml2AstParser parser = new SimXml2AstParser();
		Sprite sprite = (Sprite) parser.parse(xmlStr);
		Program program = programOf(sprite);
		return program;
	}

	public SimpProgramHelper fromSprite(Sprite sprite) {
		return new SimpProgramHelper(programOf(sprite));
	}

}
