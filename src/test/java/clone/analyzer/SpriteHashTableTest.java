package clone.analyzer;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Test;

import com.google.common.collect.Lists;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

import ast.Program;
import ast.Sprite;
import ast.Stage;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;

@Slf4j
public class SpriteHashTableTest {

	@Test
	public void testFragmentTable() {
		Program program = new Program();
		program.setStage(new Stage());
		/*
		 * sprite s1 = s2 with different script ordering; s1 has one non event starting
		 * block which is not used in clone computation
		 * sprite s3 is different from s1 & s2 by one script
		 */
		String xmlStr1 = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables></variables><block type='motion_movesteps' id='keLT!dy[oorM+K*[EMF6' x='410' y='-107'><value name='STEPS'><shadow type='math_number' id='LW3mijO}chlPu$[g##RG'><field name='NUM'>10</field></shadow></value></block><block type='event_whenflagclicked' id='3F}I*Y$9;`M:o[QI0Q{o' x='0' y='0'><next><block type='looks_show' id='I6#$:CW|81~h#tNK?uUd'></block></next></block><block type='event_whenthisspriteclicked' id='tEQRA~aKUVB;t*t22T?(' x='0' y='152'><next><block type='looks_hide' id='4*S;U5krA/PW)?q7U2h6'></block></next></block></xml>";
		Sprite s1 = new Sb3XmlParser().parseTarget(xmlStr1);
		s1.setName("sprite1");
		String xmlStr2 = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables></variables><block type='event_whenthisspriteclicked' id='D7khVcB,VRO@^#G(7O#W' x='295' y='155'><next><block type='looks_hide' id='P!/L2XmuypqE_JP4t(MK'></block></next></block><block type='event_whenflagclicked' id='GI#B`#MIg*s{U.r[fa4.' x='311' y='325'><next><block type='looks_show' id='Na8=sa`3]mreqL}Uzvr]'></block></next></block></xml>";
		Sprite s2 = new Sb3XmlParser().parseTarget(xmlStr2);
		s2.setName("sprite2");
		String xmlStr3 = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables></variables><block type='event_whenflagclicked' id='GI#B`#MIg*s{U.r[fa4.' x='-89' y='172'><next><block type='motion_movesteps' id='g|_m3X.E0!%$BM7A_.NV'><value name='STEPS'><shadow type='math_number' id=';$^;/S4u0+[ZEZ$(s}3D'><field name='NUM'>10</field></shadow></value></block></next></block><block type='event_whenthisspriteclicked' id='D7khVcB,VRO@^#G(7O#W' x='-91' y='360'><next><block type='looks_hide' id='P!/L2XmuypqE_JP4t(MK'></block></next></block></xml>";
		Sprite s3 = new Sb3XmlParser().parseTarget(xmlStr3);
		s3.setName("sprite3");
		program.getStage().addSprite(s1);
		program.getStage().addSprite(s2);
		program.getStage().addSprite(s3);

		log.debug(s1.getScript(0).toDevString());
		assertThat(s1.getScript(0).getBody().getStmt(0).isEventBlock(), is(false));

		SpriteHash seqSet1 = new SpriteHash(s1);
		assertThat(seqSet1.stmtSeqSet.size(), equalTo(2));
		SpriteHash seqSet2 = new SpriteHash(s2);
		assertThat(seqSet2.stmtSeqSet.size(), equalTo(2));
		SpriteHash seqSet3 = new SpriteHash(s3);

		assertThat(s1.getNumScript(), not(equalTo(s2.getNumScript())));
		assertThat(seqSet1.hash(), equalTo(seqSet2.hash()));
		assertThat(seqSet1.hash(), not(equalTo(seqSet3.hash())));
		assertThat(seqSet2.hash(), not(equalTo(seqSet3.hash())));

		Collection<Sprite> sprites = Lists.newArrayList(program.getSpriteList());
		SpriteHashTable table = SpriteHashTable.of(sprites);
		assertThat(table.get().keySet().size(), is(1));
		log.debug("{}", table.get().get(seqSet1.hash()));

	}

	@Test
	public void testHashOrderInsensitive() {
		int hashcode = Lists.newArrayList(123, 456).stream()
				.reduce(0, (left, right) -> left ^ right);

		log.debug("{}", hashcode);

		int hashcode2 = Lists.newArrayList(456, 123).stream()
				.reduce(0, (left, right) -> left ^ right);

		log.debug("{}", hashcode2);

	}

}
