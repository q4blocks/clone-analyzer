package clone.analyzer;

import ast.Expr;
import ast.Program;
import ast.Sprite;
import ast.Stage;

public interface AbstractProgramHelper {
	public Program fromSprite(String spriteStr);
	default Program programOf(Sprite sprite) {
		Program program = new Program();
		program.setStage(new Stage());
		program.getStage().addSprite(sprite);
		return program;
	}
}
