package clone.analyzer;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import ast.BlockSeq;
import ast.Expr;
import ast.OperatorExpr;
import ast.Program;
import ast.Script;
import ast.Sprite;
import ast.Stage;
import clone.analyzer.CloneExprAnalyzer;
import clone.data.CExpr;
import clone.result.CloneExprResult;
import sb3.dsl.SimXml2AstParser;
import sb3.parser.xml.Sb3XmlParser;

public class TestCloneExprAnalyzer {
	private static Logger LOGGER = null;
	CloneExprAnalyzer analyzer;
	SimXml2AstParser simpleParser;
	Sb3XmlParser sb3Parser;

	@BeforeClass
	public static void setLogger() throws MalformedURLException {
		LOGGER = LogManager.getLogger();
		Configurator.setRootLevel(Level.OFF);
	}

	@Before
	public void setup() {
		analyzer = new CloneExprAnalyzer();
		simpleParser = new SimXml2AstParser();
		sb3Parser = new Sb3XmlParser();
	}

	@Test
	public void testLookupNodeById1() {
		Sprite sprite = (Sprite) simpleParser
				.parse("<sprite><script><stmt><expr type='op1'><expr id='e1'></expr></expr></stmt></script></sprite>");
		Program program = new SimpProgramHelper().programOf(sprite);
		Optional<OperatorExpr> res = program.lookupOpExpr("e1");
		assertThat(res.isPresent(), equalTo(true));
	}

	@Test
	public void testLookupNodeById2() {
		String xml = "<xml xmlns=\"http://www.w3.org/1999/xhtml\"><variables></variables><block type=\"motion_movesteps\" id=\"UgnwT`Z@JQw{^2,Cp;,1\" x=\"91\" y=\"229\"><value name=\"STEPS\"><shadow type=\"math_number\" id=\"s!w)]AOedHdf`b4Ow_`h\"><field name=\"NUM\">10</field></shadow><block type=\"operator_random\" id=\":DET^_^}^2=Gx`!O?Oav\"><value name=\"FROM\"><shadow type=\"math_number\" id=\"8[:+(oyL_ENSF]=`T::k\"><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><shadow type=\"math_number\" id=\"x6`};e]aQpoSVksZ)h,8\"><field name=\"NUM\">10</field></shadow><block type=\"operator_add\" id=\"R0qLjWWMPM/fjzh8V0Ey\"><value name=\"NUM1\"><shadow type=\"math_number\" id=\"~@gTHYxXNbTie0*p6V$|\"><field name=\"NUM\">2</field></shadow></value><value name=\"NUM2\"><shadow type=\"math_number\" id=\"ywzcda=Thgqpe06k8R^j\"><field name=\"NUM\">3</field></shadow></value></block></value></block></value></block><block type=\"motion_pointindirection\" id=\"M8`Kg9?n}Sak=yAmg=DC\" x=\"567\" y=\"228\"><value name=\"DIRECTION\"><shadow type=\"math_angle\" id=\"%`8dP~]zytWP_?NoZ|nx\"><field name=\"NUM\">90</field></shadow><block type=\"operator_random\" id=\"QJ^GU))@8jM8D%w[BdiT\"><value name=\"FROM\"><shadow type=\"math_number\" id=\"WsPZy-H/5Xa:7h00C-h/\"><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><shadow type=\"math_number\" id=\"4-4pg8Wu;PwyJmuGRdoi\"><field name=\"NUM\">10</field></shadow><block type=\"operator_add\" id=\"*~5$psbD8[|5yQX#,bQ{\"><value name=\"NUM1\"><shadow type=\"math_number\" id=\"R06#2rC?q|Q0S:/Ic76O\"><field name=\"NUM\">2</field></shadow></value><value name=\"NUM2\"><shadow type=\"math_number\" id=\"#}SU!@mb1^e|;52HlfA#\"><field name=\"NUM\">3</field></shadow></value></block></value></block></value></block></xml>";
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(xml);
		Program program = new SimpProgramHelper().programOf(sprite);
		Optional<OperatorExpr> res = program.lookupOpExpr(":DET^_^}^2=Gx`!O?Oav");
		assertThat(res.isPresent(), equalTo(true));
	}

	@Test
	public void testRootExprTree() {
		Sprite sprite = (Sprite) simpleParser.parse(
				"<sprite><script><stmt><expr id='1' type='a'><expr id='1.1' type='b'></expr><expr id='1.2' type='c'><expr id='1.2.1' type='d'></expr></expr></expr></stmt></script></sprite>");
		Program program = new SimpProgramHelper().programOf(sprite);
		OperatorExpr rootExpr = program.lookupOpExpr("1").get();
		OperatorExpr child1_1 = program.lookupOpExpr("1.1").get();
		OperatorExpr child1_2_1 = program.lookupOpExpr("1.2.1").get();
		assertThat(rootExpr.isRootExpr(), equalTo(true));
		assertThat(child1_1.rootExpr(), equalTo(rootExpr));
		assertThat(child1_2_1.rootExpr(), equalTo(rootExpr));
	}

	@Test
	public void testSize() {
		Expr expr1 = (Expr) simpleParser.parse("<expr><num>1</num><expr><num>4</num></expr></expr>");
		Expr expr2 = (Expr) simpleParser.parse("<expr><expr><num>4</num></expr></expr>");
		assertTrue(new CExpr(expr1).size() > new CExpr(expr2).size());
	}

	@Test
	public void testCloneExpr() {
		SimXml2AstParser parser = new SimXml2AstParser();
		Sprite sprite = (Sprite) parser.parse(
				"<sprite><script><stmt><expr id='1' type='a'><expr id='2' type='b'><num>1</num></expr></expr></stmt><stmt><expr id='3' type='a'><expr id='4' type='b'><num>1</num></expr></expr></stmt></script></sprite>");

		Map<Expr, List<CExpr>> res = analyzer.analyze(new SimpProgramHelper().programOf(sprite));
		LOGGER.debug(res);

		CloneExprResult result = new CloneExprResult(res);
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
		assertThat(result.firstGroup().firstInstance().asNameSeq(), contains("a", "b", "1"));
	}

	@Test
	public void testCloneExpr2() {
		String xmlStr = "<xml><variables></variables><block type='motion_movesteps' id='(Q5ph7?vGm+:/6wC]44E' x='148' y='147'><value name='STEPS'><shadow type='math_number' id='tJu~}j$qp}d9;%q=5B5{'><field name='NUM'>10</field></shadow><block type='operator_random' id='Rt8Fds77WpFZ44Z;0=q['><value name='FROM'><shadow type='math_number' id='gB2~A2ss^%!Lm|4H@kZ9'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='math_number' id='ZkILx,~,|xh?5RJJc:}*'><field name='NUM'>10</field></shadow><block type='operator_length' id='3v)AY`/QYw=YumEO-2ph'><value name='STRING'><shadow type='text' id='y*r2Mb!Xx3ux5o{c4:Qa'><field name='TEXT'>world</field></shadow></value></block></value></block></value><next><block type='motion_turnright' id='JK|77ipA{}kp%5bJVb;d'><value name='DEGREES'><shadow type='math_number' id='as%)4njC~$l;7,du^`D{'><field name='NUM'>15</field></shadow><block type='operator_letter_of' id='zbyM3^7S!!?1+?[jV`kX'><value name='LETTER'><shadow type='math_whole_number' id='=u6ezH}BzGpVZ2DWM?3B'><field name='NUM'>1</field></shadow><block type='operator_random' id='bUJ5K1q`VqvCJ;6djImb'><value name='FROM'><shadow type='math_number' id='(L`b8@M_$w-~|Oc8{_KG'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='math_number' id='qA7:-HV0D{aRS+%2m[IB'><field name='NUM'>10</field></shadow><block type='operator_length' id=',TClb}*}0E;Ao=4txi`G'><value name='STRING'><shadow type='text' id='_m!2/g+]WFu$6vLRC`W!'><field name='TEXT'>world</field></shadow></value></block></value></block></value><value name='STRING'><shadow type='text' id='w1(W!@K2;fImc]7hE}wy'><field name='TEXT'>world</field></shadow></value></block></value></block></next></block></xml>";
		System.out.println(xmlStr);
		Sprite sprite = sb3Parser.parseTarget(xmlStr);

		Program program = new SimpProgramHelper().programOf(sprite);
		System.out.println(program.print());
		Map<Expr, List<CExpr>> res = analyzer.analyze(program);
		CloneExprResult result = new CloneExprResult(res);

		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
		LOGGER.debug(result.firstGroup().firstInstance().asNameSeq());
		assertThat(result.firstGroup().firstInstance().asNameSeq(),
				contains("operator_random", "1", "operator_length", "world"));
	}

	// clones are in separate target; may allow later for global analysis
	@Ignore
	@Test
	public void testCloneExpr3() {
		String xml = "<program><stage name=\"Stage\"><xml><variables><variable type=\"\" id=\"B)Sa^NF8*FRfBi=]*VG}-a\">a</variable></variables><block id=\"~D=_aD+3G}c^PQ(t*.#Q\" type=\"looks_nextbackdrop\" x=\"117\" y=\"327.8\" ></block></xml></stage><sprite name=\"Sprite1\"><xml><variables><variable type=\"\" id=\"C+~@[SO+Pq];7nvT[xsW-b\">b</variable></variables><block id=\"y@JhECx|pJ{Hs5wi(O^1\" type=\"motion_movesteps\" x=\"286.5\" y=\"349.8\" ><value name=\"STEPS\"><block id=\"O#ydaL2ZG*YXAn,arG=_\" type=\"operator_random\" ><value name=\"FROM\"><shadow id=\"gQ;OM:w2KK7}`aKrMBv5\" type=\"math_number\" ><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><block id=\".HG1;dcIK~=~c_w+v+ax\" type=\"operator_add\" ><value name=\"NUM1\"><block id=\"W!{^U#b1Wb5GqG#Ekia`\" type=\"operator_multiply\" ><value name=\"NUM1\"><shadow id=\"+IW2hoYVy3?DTxz%?]mB\" type=\"math_number\" ><field name=\"NUM\">1</field></shadow></value><value name=\"NUM2\"><shadow id=\"81)6Q:uS~aF#/+er-R`@\" type=\"math_number\" ><field name=\"NUM\">2</field></shadow></value></block><shadow id=\"Fnvo4%puG?@#iz-kHNhR\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value><value name=\"NUM2\"><block id=\"tvAcMOW_/B./wFu6HB5.\" type=\"operator_mod\" ><value name=\"NUM1\"><shadow id=\"+3Y/s5;E_p?iG23/Vh[b\" type=\"math_number\" ><field name=\"NUM\">24</field></shadow></value><value name=\"NUM2\"><shadow id=\"Qob*5zf2)QgDoK7m_vYg\" type=\"math_number\" ><field name=\"NUM\">5</field></shadow></value></block><shadow id=\"EDVX]k-2l#2m[o5*FxQz\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block><shadow id=\"O^^IVL_||,ALO}/bmLT?\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block><shadow id=\"YyP1s#43~:4;CByl[iYm\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block></xml></sprite><sprite name=\"Sprite2\"><xml><variables></variables><block id=\"chbxMdNI[ac!L=p!Bt9)\" type=\"motion_turnright\" x=\"313.5\" y=\"349.8\" ><value name=\"DEGREES\"><block id=\"X`ZqKapKVc=uTQsR+PUO\" type=\"operator_random\" ><value name=\"FROM\"><shadow id=\"w9/.A(YNO-/CrATm{mQ0\" type=\"math_number\" ><field name=\"NUM\">2</field></shadow></value><value name=\"TO\"><block id=\"p`GIKU{/F7eq1.w6V5DN\" type=\"operator_add\" ><value name=\"NUM1\"><block id=\"M{thUPjRz[xktb/,b)q#\" type=\"operator_multiply\" ><value name=\"NUM1\"><shadow id=\"IP#A%Wf//yR}y61OI]CI\" type=\"math_number\" ><field name=\"NUM\">1</field></shadow></value><value name=\"NUM2\"><shadow id=\"I~YwboCM/m}TMVW`;x2_\" type=\"math_number\" ><field name=\"NUM\">2</field></shadow></value></block><shadow id=\"XEes=O1Xg+vYEP1{#K;e\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value><value name=\"NUM2\"><block id=\"C*]IwhD4.,EC4f:gdV;C\" type=\"operator_mod\" ><value name=\"NUM1\"><shadow id=\"cb)eb`}wqroE)5pOKRqg\" type=\"math_number\" ><field name=\"NUM\">24</field></shadow></value><value name=\"NUM2\"><shadow id=\"|bvsVSiO5|+d}8:(K@w7\" type=\"math_number\" ><field name=\"NUM\">5</field></shadow></value></block><shadow id=\".q:,7U/zht0jOZ4Iu_nl\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block><shadow id=\"8T/]/=gKo4*`A_YtX.ig\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block><shadow id=\"M`cK(kG81%FgubH!7bd6\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block></xml></sprite></program>";
		Program program = sb3Parser.parseProgram(xml);
		Map<Expr, List<CExpr>> res = analyzer.analyze(program);
		CloneExprResult result = new CloneExprResult(res);
		System.out.println(program.toDevString());
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
		LOGGER.debug(result.firstGroup().firstInstance().asNameSeq());
		assertThat(result.firstGroup().firstInstance().asNameSeq(),
				contains("operator_add", "operator_multiply", "1", "2", "operator_mod", "24", "5"));
		OperatorExpr rootExpr = (OperatorExpr) result.firstGroup().firstInstance().get();
		LOGGER.debug(rootExpr.getBlockId());
	}

	/*
	 * TODO: ignore small expression
	 */

	@Test
	public void testBugs() {
		String xmlStr = "<xml><variables><variable type=\"\" id=\"`jEk@4|i[#Fk?(8x)AV.-my variable\">my variable</variable><variable type=\"\" id=\"Y})TGj3O`[./JoG:jwvt\">1234</variable></variables><block type=\"motion_movesteps\" id=\"[{q`2tD%^4ZEqvdMf0W5\" x=\"560\" y=\"181\"><value name=\"STEPS\"><shadow type=\"math_number\" id=\"4{zn0@y7uI??w2*9rJ,Q\"><field name=\"NUM\">10</field></shadow></value></block><block type=\"procedures_definition\" id=\"c(RW_Z7x8f8TpN2I8evY\" x=\"802\" y=\"257\"><statement name=\"custom_block\"><shadow type=\"procedures_prototype\" id=\"Glj5.oJQ1XLB[6m5xFZ_\"><mutation proccode=\"block name\" argumentids=\"[]\" argumentnames=\"[]\" argumentdefaults=\"[]\" warp=\"false\"></mutation></shadow></statement><next><block type=\"motion_turnright\" id=\"M?{T{,6@P8:wyMb;`X*M\"><value name=\"DEGREES\"><shadow type=\"math_number\" id=\"7h!2%Hb6=X|WoOvwy4B0\"><field name=\"NUM\">15</field></shadow><block type=\"operator_random\" id=\"$PATRqSQLB-=iFk|}*zD\"><value name=\"FROM\"><shadow type=\"math_number\" id=\"vZOGnY|$^0x}fG_R.Wqd\"><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><shadow type=\"math_number\" id=\",B9(dVxY^GpK$:Rk,HUR\"><field name=\"NUM\">10</field></shadow></value></block></value></block></next></block></xml>";
		Sprite sprite = sb3Parser.parseTarget(xmlStr);
		System.out.println(xmlStr);
		Program program = new SimpProgramHelper().programOf(sprite);
		Map<Expr, List<CExpr>> res = analyzer.analyze(program);
		CloneExprResult result = new CloneExprResult(res);
		System.out.println(result.numGroup());
	}

	@Test
	public void test() {
		String xmlStr = "<xml><variables></variables><block type='event_whenflagclicked' id='5BqI%BvkS#aTe?/yRnVl' x='269' y='105'><next><block type='control_forever' id='vfjg`@2T/{(F01A%tNK5'><statement name='SUBSTACK'><block type='motion_movesteps' id='tDcqclM9:s$Sj)))E6%?'><value name='STEPS'><shadow type='math_number' id='j4RpU{btv@#xg6+q)yE{'><field name='NUM'>10</field></shadow><block type='operator_random' id='|%1$^_anQcg1-2z.4Y7^'><value name='FROM'><shadow type='math_number' id='$)cS4y;SvywW?A!5lrPv'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='math_number' id='?L!VpU1gw^WV,|5E#)NG'><field name='NUM'>10</field></shadow><block type='operator_add' id='9_MauLGS=@6w|=@h+V+b'><value name='NUM1'><shadow type='math_number' id='T;Z*dp8Ch-j(y_ZdlWRx'><field name='NUM'>2</field></shadow></value><value name='NUM2'><shadow type='math_number' id='21EvE7WPuh1^V%lgv/gB'><field name='NUM'>3</field></shadow></value></block></value></block></value><next><block type='motion_turnright' id='X~H)z6@*svnpz1R6/mv1'><value name='DEGREES'><shadow type='math_number' id='GPbFcZ[g]z|w2}F,:h4I'><field name='NUM'>15</field></shadow><block type='operator_random' id='YfoDcYz=g$1@4kmJaNXv'><value name='FROM'><shadow type='math_number' id='R7?5ByZfNIFRc2Re?tcD'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='math_number' id='|%@4o![/TTfPiy;p8Cpf'><field name='NUM'>10</field></shadow><block type='operator_add' id='?WDwzRtw/jUQ+y6O[flu'><value name='NUM1'><shadow type='math_number' id='~KC4Lt+[*yCCrTfrHdY('><field name='NUM'>2</field></shadow></value><value name='NUM2'><shadow type='math_number' id='H_a,jaAE,VHH+4,O*=rN'><field name='NUM'>3</field></shadow></value></block></value></block></value></block></next></block></statement></block></next></block></xml>";
		Sprite sprite = sb3Parser.parseTarget(xmlStr);
		System.out.println(xmlStr);
		Program program = new SimpProgramHelper().programOf(sprite);
		Map<Expr, List<CExpr>> res = analyzer.analyze(program);
		CloneExprResult result = new CloneExprResult(res);
		System.out.println(result.firstGroup().firstInstance().get().toDevString());
	}

	@Test
	public void shouldAnalyzeCloneForEachTargetSeparately() {
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		String xmlTarget1 = "<xml><variables></variables><block type='motion_movesteps' id='1=p`mfFR!c(~x,([m*wx' x='205' y='105'><value name='STEPS'><shadow type='math_number' id='uN9FdW`1IdygM9Eg3p;{'><field name='NUM'>10</field></shadow><block type='operator_random' id='S%TI[wg~{W{//J{/@++#'><value name='FROM'><shadow type='math_number' id='SD(?GC3Fyu7VthRTAfAO'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='math_number' id='mVlO_5Yh$M$yt?fPEedH'><field name='NUM'>10</field></shadow><block type='operator_round' id='VyemEo?HegY9oh{M$6WZ'><value name='NUM'><shadow type='math_number' id='ON7.ry2^/ZWK=!nC;RKp'><field name='NUM'>3</field></shadow></value></block></value></block></value></block></xml>";
		String xmlTarget2 = "<xml><variables></variables><block type='motion_turnright' id='hGlBG%zDePrSYm,56(Jp' x='305' y='157'><value name='DEGREES'><shadow type='math_number' id='C!3V$[NGASKh*sw=oH4p'><field name='NUM'>15</field></shadow><block type='operator_random' id='f6f(]s[x]iTtJemGE0ow'><value name='FROM'><shadow type='math_number' id='JG0iE0)HRwJN#UYA2G!9'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='math_number' id=',Dl#0z+2(0.M8Thx.my%'><field name='NUM'>10</field></shadow><block type='operator_round' id='MiiF^V:^RH=iE48}h9Ae'><value name='NUM'><shadow type='math_number' id='4bv(sSBzp|L|[O|)$d;:'><field name='NUM'>3</field></shadow></value></block></value></block></value><next><block type='motion_movesteps' id='}-RD!5+UAaH0$3`lCW1)'><value name='STEPS'><shadow type='math_number' id='3!L(7QPhW16!|`5#5+=X'><field name='NUM'>10</field></shadow><block type='operator_random' id='gh|tomm=gf{QG#F,WTkp'><value name='FROM'><shadow type='math_number' id='F9ob*$zv!%[=PrRtK4N4'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='math_number' id='7.;AovI@?QH?g{F}A%?S'><field name='NUM'>10</field></shadow><block type='operator_round' id='A`5lo]#V34l);a{OFF+F'><value name='NUM'><shadow type='math_number' id=';txo/TL?*#]ORHds0ZD%'><field name='NUM'>3</field></shadow></value></block></value></block></value></block></next></block></xml>";
		Sprite sprite1 = sb3Parser.parseTarget(xmlTarget1);
		sprite1.setName("sprite1");
		Sprite sprite2 = sb3Parser.parseTarget(xmlTarget2);
		sprite2.setName("sprite2");
		Program program = new Program();
		program.setStage(new Stage());
		program.getStage().addSprite(sprite1);
		program.getStage().addSprite(sprite2);
		Map<Expr, List<CExpr>> res = analyzer.analyze(program);
		CloneExprResult result = new CloneExprResult(res);
		System.out.println(result);
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
	}

	@Test
	public void shouldAnalyzeCloneForEachScriptSeparately() {
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		String xmlTarget1 = "<xml><variables></variables><block type='motion_movesteps' id='EzRuDcwJ+c1tv3cc.#i^' x='187' y='243'><value name='STEPS'><shadow type='math_number' id='uA4rSn=dBO,,_0fbqb)u'><field name='NUM'>10</field></shadow><block type='operator_add' id='oG_kFkUZJ(`6_beO}`]v'><value name='NUM1'><shadow type='math_number' id='8VrFR^oM*)P*PsHv*l2U'><field name='NUM'>1</field></shadow></value><value name='NUM2'><shadow type='math_number' id='?=FjL$o{+uy4=L8,k!ua'><field name='NUM'></field></shadow><block type='operator_subtract' id='fI2bq+sFg~qI_Rl04yww'><value name='NUM1'><shadow type='math_number' id='?/14c9p`A.;(2(NU}Vip'><field name='NUM'>3</field></shadow></value><value name='NUM2'><shadow type='math_number' id='J_jqde(x[2*q8/{GK|D0'><field name='NUM'>2</field></shadow></value></block></value></block></value></block><block type='motion_turnright' id='t9~k_HlJ-jtd=p9KA1z8' x='235' y='420'><value name='DEGREES'><shadow type='math_number' id='GtwsOM~75]pPW!adn~y!'><field name='NUM'>15</field></shadow><block type='operator_add' id='XJEe~{coj[IQF%)?2DlT'><value name='NUM1'><shadow type='math_number' id='LjZ{LBTCKm#6bo7r7THy'><field name='NUM'>1</field></shadow></value><value name='NUM2'><shadow type='math_number' id='=;XT|*U#,%1JN.WxB!x{'><field name='NUM'></field></shadow><block type='operator_subtract' id='vm*%?q2crwihmti,l=_.'><value name='NUM1'><shadow type='math_number' id='QweH(x3m.E2$p=xP4^`9'><field name='NUM'>3</field></shadow></value><value name='NUM2'><shadow type='math_number' id='7(?ka*6an+0;K,C0dYp}'><field name='NUM'>2</field></shadow></value></block></value></block></value></block></xml>";

		Sprite sprite1 = sb3Parser.parseTarget(xmlTarget1);
		sprite1.setName("sprite1");
		Program program = new Program();
		program.setStage(new Stage());
		program.getStage().addSprite(sprite1);

		for (BlockSeq seq : sprite1.rootSeqColl()) {
			Map<Expr, List<CExpr>> res = analyzer.analyze(seq);
			CloneExprResult result = new CloneExprResult(res);
			assertThat(result.numGroup(), equalTo(0));
		}
	}

	@Test
	public void fullOfTypeAndNameForVarAccessThatCannotBeReplaced() {
		String xml = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables><variable type='list' id='XR^/.f;rHM(OrRNuMl`U' islocal='false'>a</variable><variable type='list' id='OvqQh.KZ+d;Q6Ma(!}JE' islocal='false'>b</variable><variable type='' id='RZz)eE4c;6*@|fO$N/)v' islocal='false'>w</variable></variables><block type='motion_gotoxy' id='V5OU-Fs}SYb1+fOQ#aq~' x='60' y='24'><value name='X'><shadow type='math_number' id='Ah,1[C5Kj?D2kUUqJxx8'><field name='NUM'>0</field></shadow><block type='data_itemoflist' id='ZYi)SNECYRRA{,sYfYs.'><field name='LIST' id='XR^/.f;rHM(OrRNuMl`U' variabletype='list'>a</field><value name='INDEX'><shadow type='math_integer' id='peHt//Vvh8;=Ar:1d{aD'><field name='NUM'>1</field></shadow><block type='operator_round' id='Kzb]@_WW+6$=c*?Tx@8r'><value name='NUM'><shadow type='math_number' id='|WFN::1rNPCLFJbL!=?t'><field name='NUM'>1</field></shadow></value></block></value></block></value><value name='Y'><shadow type='math_number' id='8Tg3vdnYYDZP%.6LgY;u'><field name='NUM'>0</field></shadow><block type='data_itemoflist' id='58T7R1VnIagyb~=VP5sL'><field name='LIST' id='XR^/.f;rHM(OrRNuMl`U' variabletype='list'>a</field><value name='INDEX'><shadow type='math_integer' id='KkD.ECAA)_[qJ4nLc+p)'><field name='NUM'>2</field></shadow><block type='operator_round' id='yG.YSn5AJX{@2e)L:VT-'><value name='NUM'><shadow type='math_number' id='u-5OmjJyG|-Db$S?oP;H'><field name='NUM'>1</field></shadow></value></block></value></block></value><next><block type='motion_movesteps' id=')jCK`KKu)`S/|_N#MPQv'><value name='STEPS'><shadow type='math_number' id='2d/8Nnpq,N[CvZ*bX09n'><field name='NUM'>10</field></shadow><block type='data_itemoflist' id='K/vM1MlQF8QQ;bHNPW~~'><field name='LIST' id='OvqQh.KZ+d;Q6Ma(!}JE' variabletype='list'>b</field><value name='INDEX'><shadow type='math_integer' id='Hlwc]sM.!$3~-c^*J=g^'><field name='NUM'>2</field></shadow><block type='operator_round' id='4r)[%vpf8F@CSafV6p#f'><value name='NUM'><shadow type='math_number' id=']~ykO_44NCJH^#`_s1u^'><field name='NUM'>2</field></shadow></value></block></value></block></value></block></next></block></xml>";
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(xml);
		Program program = new SimpProgramHelper().programOf(sprite);
		CloneExprAnalyzer analyzer = new CloneExprAnalyzer();
		Map<Expr, List<CExpr>> res = analyzer.analyze(program);
		CloneExprResult result = new CloneExprResult(res);
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
	}

	@Test
	public void fixBugVarName() {
		String xml = "<xml><variables><variable type='' id='CM+vwe::_dHS^=YXYnz*' islocal='false'>a</variable><variable type='' id='d9=T7G}5B%KFfK4#sq@f' islocal='false'>b</variable></variables><block type='motion_gotoxy' id='q[jG|M25aAc-tBc+4H|.' x='103' y='159'><value name='X'><shadow type='math_number' id='xvwD,)G,zybF`ut~y@7%'><field name='NUM'>0</field></shadow><block type='operator_add' id='.$6EiK*oJoItW;j[;/aX'><value name='NUM1'><shadow type='math_number' id='ZZ7.ihhA[9$uryvogD$Q'><field name='NUM'></field></shadow><block type='data_variable' id='/DZCpSt%nOh!m;n[Qzkx'><field name='VARIABLE' id='CM+vwe::_dHS^=YXYnz*' variabletype=''>a</field></block></value><value name='NUM2'><shadow type='math_number' id='r~qTg?BREJJ+:(uE1n)b'><field name='NUM'>1</field></shadow></value></block></value><value name='Y'><shadow type='math_number' id='=0!,jl6e|*168DfD^~V5'><field name='NUM'>0</field></shadow><block type='operator_add' id='~gCbu{)aM;jcioJl@qQF'><value name='NUM1'><shadow type='math_number' id='/V=f=;nXId[Hqj.2NWQ;'><field name='NUM'></field></shadow><block type='data_variable' id='S,Z6GA:FsBsuPz{yBxy|'><field name='VARIABLE' id='d9=T7G}5B%KFfK4#sq@f' variabletype=''>b</field></block></value><value name='NUM2'><shadow type='math_number' id='}3%]7l%M8r?M!n{@-.xA'><field name='NUM'>1</field></shadow></value></block></value></block></xml>";
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(xml);
		Program program = new SimpProgramHelper().programOf(sprite);
		Script script = sprite.getScript(0);
		Map<Expr, List<CExpr>> res = analyzer.analyze(script.getBody());
		CloneExprResult result = new CloneExprResult(res);
		System.out.println(result);
		assertThat(result.numGroup(), equalTo(0));
	}
	
	@Test
	public void fixBug() {
		String xml = "<xml><variables></variables><block type='motion_gotoxy' id='?##}=@U2_6aQ~(Bo/I,{'><value name='X'><shadow id='?JwB}l!Ai{st;Bw;;VWq' type='math_number'><field name='NUM'>10</field></shadow><block type='operator_add' id='@2MOu0-azM3%6y7i}mmE'><value name='NUM1'><shadow id='tH_4G6oXHGTNd-^Ys!6B' type='math_number'><field name='NUM'>10</field></shadow><block type='argument_reporter_string_number' id='4cwL,2@XQgTPDFL7fcd)'><field name='VALUE'>x</field></block></value><value name='NUM2'><shadow type='math_number' id='G+.CxqY}RBR1uXF1eyXX'><field name='NUM'>30</field></shadow></value></block></value><value name='Y'><shadow id='vU~5gu;sX%rc%G?;fV`=' type='math_number'><field name='NUM'>10</field></shadow><block type='operator_add' id='s#O=xxp3#Z*+169%^0:k'><value name='NUM1'><shadow id='t)nYhYL*-RL?X9|)R={]' type='math_number'><field name='NUM'>10</field></shadow><block type='argument_reporter_string_number' id='3?nh}2QT}+5E,PfjT|^~'><field name='VALUE'>y</field></block></value><value name='NUM2'><shadow type='math_number' id=']h]Dcp)lCR^ij#%1b#}}'><field name='NUM'>20</field></shadow></value></block></value></block></xml>";
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(xml);
		Program program = new SimpProgramHelper().programOf(sprite);
		Script script = sprite.getScript(0);
		Map<Expr, List<CExpr>> res = analyzer.analyze(script.getBody());
		CloneExprResult result = new CloneExprResult(res);
		System.out.println(result);
		assertThat(result.numGroup(), equalTo(0));
	}
}
