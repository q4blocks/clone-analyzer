package clone.analyzer;

import ast.Program;
import sb3.parser.xml.Sb3XmlParser;

public class ScratchProgramHelper implements AbstractProgramHelper {
	private Sb3XmlParser sb3Parser = new Sb3XmlParser();
	private Program program;

	public ScratchProgramHelper(Program program) {
		this.program = program;
	}

	public ScratchProgramHelper() {
		
	}

	public Program getProgram() {
		return program;
	}

	public Program fromSprite(String spriteStr) {
		return programOf(sb3Parser.parseTarget(spriteStr));
	}

}
