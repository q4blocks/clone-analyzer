package clone.result;

import java.util.List;
import java.util.stream.Collectors;

import ast.ExprStmt;
import ast.Stmt;
import clone.data.StmtSeq;

public class CloneSeq {

	private final StmtSeq seq;

	public CloneSeq(StmtSeq ccStmtSeq) {
		this.seq = ccStmtSeq;
	}

	public int size() {
		return seq.size();
	}
	
	public int numTotalStmt() {
		return seq.numTotalStmt();
	}

	public List<Stmt> get() {
		return seq.get();
	}

	public List<String> asNameSeq() {
		List<String> names = seq.get().stream().map(stmt -> stmt.getOpcode())
				.collect(Collectors.toList());
		return names;
	}
}