package clone.result;

import static java.util.stream.Collectors.groupingBy;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import ast.ASTNode;
import ast.BlockSeq;
import ast.Expr;
import ast.ExprStmt;
import ast.Literal;
import ast.OperatorExpr;
import clone.analyzer.SerializationConfig;
import clone.analyzer.ASTNodeSerializer;
import clone.data.CExpr;
import clone.data.StmtSeq;

public class CloneExprResult {

	private final List<CloneGroup> cloneGroups;

	public CloneExprResult(Map<Expr, List<CExpr>> res) {
		Map<Integer, List<CExpr>> groupByHash = res.values().stream().flatMap(Collection::stream)
				.collect(groupingBy(CExpr::hash));
		cloneGroups = groupByHash.values().stream().map(CloneGroup::new).collect(Collectors.toList());
	}

	public static CloneExprResult of(Map<Expr, List<CExpr>> res) {
		return new CloneExprResult(res);
	}
 
	public int numGroup() {
		return cloneGroups.size();
	}

	public List<CloneGroup> groups() {
		return cloneGroups;
	}

	public CloneGroup firstGroup() {
		return this.cloneGroups.get(0);
	}

	public CloneGroup group(int i) {
		return cloneGroups.get(i);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		for (int i = 0; i < numGroup(); i++) {
			sb.append("Group " + i + " " + group(i).getTarget() + "\n");
			sb.append(group(i));
			sb.append("==============\n");
		}
		return sb.toString();
	}

	public class CloneGroup {
		private final List<CExpr> group;

		public CloneGroup(List<CExpr> group) {
			this.group = group;
		}

		public int size() {
			return group.size();
		}

		public List<CExpr> get() {
			return this.group;
		}

		public CloneExpr getInstance(int i) {
			return new CloneExpr(group.get(i));
		}

		public CloneExpr firstInstance() {
			return new CloneExpr(group.get(0));
		}

		public String getTarget() {
			return firstInstance().get().enclosingStmt().sprite().getName();
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			for (CExpr seq : this.get()) {
				sb.append(seq.get().toDevString());
				sb.append("@" + seq.get().enclosingStmt().toDevString());
				sb.append("\n");
			}

			return sb.toString();
		}
	}

	public class CloneExpr {

		@Override
		public String toString() {
			return "CloneExpr [" + (expr != null ? expr.get().toDevString() : "") + "]";
		}

		private final CExpr expr;

		public CloneExpr(CExpr expr) {
			this.expr = expr;
		}

		public Expr get() {
			return expr.get();
		}

		public int size() {
			return expr.size();
		}

		public List<String> asNameSeq() {
			SerializationConfig config = new SerializationConfig.Builder().skipField(false).skipInput(false).build();
			List<ASTNode> serialized = new ASTNodeSerializer(config).serialize(expr.get());
			List<String> names = serialized.stream().map(expr -> (Expr) expr).map(expr -> {
				if (expr instanceof OperatorExpr) {
					return ((OperatorExpr) expr).getOpcode();
				} else if (expr instanceof Literal) {
					return ((Literal) expr).getValue();
				} else {
					return expr.getClass().getSimpleName();
				}
			}).collect(Collectors.toList());
			return names;
		}

	}

}
