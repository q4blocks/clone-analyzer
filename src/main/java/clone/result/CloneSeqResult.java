package clone.result;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import ast.BlockSeq;
import ast.ExprStmt;
import ast.Program;
import ast.Scriptable;
import ast.Stmt;
import clone.data.StmtSeq;
import clone.result.CloneExprResult.CloneExpr;

public class CloneSeqResult<T> {

	private Multimap<T, CloneGroup> resultMultimap;
	private List<CloneGroup> cloneGroups;

	public CloneSeqResult() {

	}

	public CloneSeqResult(Map<T, List<StmtSeq>> res) {
		Map<Integer, List<StmtSeq>> groupByHash = res.values().stream().flatMap(Collection::stream)
				.collect(groupingBy(StmtSeq::hash));

		// elimiate group with 1 sequence
		groupByHash = groupByHash.entrySet().stream().filter(e -> e.getValue().size() > 1)
				.collect(toMap(Entry::getKey, Entry::getValue));

		cloneGroups = groupByHash.values().stream().map(CloneGroup::new).collect(Collectors.toList());
	}

	public CloneSeqResult(Multimap<T, CloneGroup> resultMultimap) {
		this.resultMultimap = resultMultimap;
		cloneGroups = resultMultimap.asMap().values().stream().flatMap(Collection::stream).collect(Collectors.toList());
	}

	public CloneSeqResult<T> of(Multimap<T, List<StmtSeq>> cloneMap) {
		Multimap<T, CloneGroup> resultMultimap = ArrayListMultimap.create();
		for (Entry<T, List<StmtSeq>> entry : cloneMap.entries()) {
			// todo
			Map<Integer, List<StmtSeq>> groupByHash = entry.getValue().stream().collect(groupingBy(StmtSeq::hash));

			// elimiate group with 1 sequence
			groupByHash = groupByHash.entrySet().stream().filter(e -> e.getValue().size() > 1)
					.collect(toMap(Entry::getKey, Entry::getValue));

			List<CloneGroup> cloneGroupList = groupByHash.values().stream().map(CloneGroup::new)
					.collect(Collectors.toList());
			resultMultimap.putAll(entry.getKey(), cloneGroupList);
		}
		return new CloneSeqResult<T>(resultMultimap);
	}

	public int numGroup() {
		return cloneGroups.size();
	}

	public List<CloneGroup> groups() {
		return cloneGroups;
	}

	public CloneGroup group(int i) {
		return cloneGroups.get(i);
	}

	public CloneGroup firstGroup() {
		return this.cloneGroups.get(0);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		for (int i = 0; i < numGroup(); i++) {
			sb.append("Group " + i + " " + group(i).getTarget() + "\n");
			sb.append(group(i));
			sb.append("==============\n");
		}
		return sb.toString();
	}

}
