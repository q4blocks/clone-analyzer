package clone.result;

import java.util.List;
import java.util.stream.Collectors;

import ast.ExprStmt;
import ast.Stmt;
import clone.data.StmtSeq;

public class CloneGroup {
	private final List<StmtSeq> group;
	String targetName;

	public CloneGroup(List<StmtSeq> group) {
		this.group = group;
	}

	public int size() {
		return group.size();
	}

	public CloneSeq firstPart() {
		return new CloneSeq(group.get(0));
	}

	public CloneSeq getPart(int i) {
		return new CloneSeq(group.get(i));
	}

	public List<StmtSeq> get() {
		return group;
	}

	public void setTarget(String name) {
		this.targetName = name;
	}

	public String getTarget() {
		if (this.targetName == null) {
			return firstPart().get().get(0).sprite().getName();
		}
		return this.targetName;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (StmtSeq seq : this.get()) {
			seq.get().forEach(stmt -> sb.append(stmt.toDevString() + "\n"));
			sb.append("---\n");
		}

		return sb.toString();
	}
}



