package clone.data;

/*
 * interface for candidate element wrapper
 */
public interface CandidateExpr<T> {
	public T get();

	public int size();
	
	public int hash();

	public T getParent();

	public void setParent(T parent); // to remove //immutable set at creation

	public int startingIndex();	//should be lenght from rootExpr
	
	public static int priority(CandidateExpr<?> elm1, CandidateExpr<?> elm2) {
		int seqPriority = elm1.size() - elm2.size();
		if (seqPriority == 0) {
			seqPriority = elm2.startingIndex() - elm1.startingIndex();
		}
		return seqPriority;
	}

}
