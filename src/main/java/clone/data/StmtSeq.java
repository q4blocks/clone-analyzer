package clone.data;

import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import ast.ASTNode;
import ast.BlockSeq;
import ast.Expr;
import ast.Stmt;
import clone.analyzer.SerializationConfig;
import clone.analyzer.ASTNodeHasher;
import clone.analyzer.ASTNodeSerializer;

/*
 *  ASTStmt collectors:
 * 	collect a range of seq of statements from all BlockSeq (containing valid flow thru sequence of blocks) and apply sliding windows
 * Wrap in CCStmtSeq
 */

public class StmtSeq {
	private final BlockSeq parent;
	private final List<Stmt> subseq;

	public StmtSeq(List<Stmt> subseq, BlockSeq parent) {
		this.subseq = subseq;
		this.parent = parent;
	}

	public List<Stmt> get() {
		return subseq;
	}

	public int size() {
		return subseq.size();
	}
	
	public int numTotalStmt() {
		SerializationConfig config = new SerializationConfig.Builder().skipField(true).skipInput(true).build();
		List<ASTNode> serialized = new ASTNodeSerializer(config).serializeASTSeq(subseq);
		List<ASTNode> stmtColl = serialized.stream().filter(n -> n instanceof Stmt && !(n instanceof BlockSeq))
				.collect(Collectors.toList());
		System.out.println(stmtColl);
		return stmtColl.size();
	}

	public int hash() {
		SerializationConfig config = new SerializationConfig.Builder().skipField(false).skipInput(true).build();
		return hash(config);
	}

	public int hash(SerializationConfig config) {
		List<ASTNode> serialized = new ASTNodeSerializer(config).serializeASTSeq(subseq);
		return new ASTNodeHasher(false).hashOf(serialized);
	}

	public BlockSeq getParent() {
		return parent;
	}

	public int startingIndex() {
		return parent.getIndexOfChild(subseq.get(0));
	}

	public static int priority(StmtSeq seq1, StmtSeq seq2) {
		int seqPriority = seq1.size() - seq2.size();
		if (seqPriority == 0) {
			seqPriority = seq2.startingIndex() - seq1.startingIndex();
		}
		return seqPriority;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Stmt stmt : subseq) {
			sb.append(stmt.toDevString());
			sb.append(",");
		}
		return sb.toString();
	}

}
