package clone.analyzer;

import static clone.analyzer.CustomCollectors.toSortedList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import ast.ASTNode;
import ast.BlockSeq;
import ast.Expr;
import ast.OperatorExpr;
import ast.Program;
import ast.Scriptable;
import clone.data.CExpr;
import clone.result.CloneExprResult;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
public class CloneExprAnalyzer {
	SerializationConfig config = new SerializationConfig.Builder().skipField(false).skipInput(false).build();
	ASTNodeSerializer exprSerializer = new ASTNodeSerializer(config);
	int minCCGroupSize = 2;
	int minCloneTreeSize = 2;

	private Function<CExpr, Integer> groupingStrategy;
	private Function<CExpr, Expr> parentLink;
	private Comparator<CExpr> seqPriorityFunc;
	private Comparator<Entry<Expr, List<CExpr>>> parentPriority;

	// TODO: Refactor to use analyze (scriptable)
	public Map<Expr, List<CExpr>> analyze(Program program) {
		Map<Expr, List<CExpr>> results = new HashMap<>();
		for (Scriptable scriptable : program.getStageAndSprites()) {
			List<CExpr> inputList = new ArrayList<>();
			HashSet<OperatorExpr> opExprSet = scriptable.opExprColl();
			if (opExprSet.isEmpty()) {
				continue;
			}
			inputList.addAll(opExprSet.stream().map(CExpr::new).filter(cloneExpr -> cloneExpr.size() >= minCloneTreeSize)
					.collect(Collectors.toList()));

			results.putAll(process(inputList));
		}

		return results;
	}

	public Map<Expr, List<CExpr>> analyze(Scriptable s) {
		List<CExpr> inputList = new ArrayList<>();
		HashSet<OperatorExpr> opExprSet = s.opExprColl();
		inputList.addAll(opExprSet.stream().map(CExpr::new).filter(cloneExpr -> cloneExpr.size() >= minCloneTreeSize)
				.collect(Collectors.toList()));
		return process(inputList);
	}

	public CloneExprResult analyze2(Scriptable s) {
		Map<Expr, List<CExpr>> res = analyze(s);
		return new CloneExprResult(res);
	}

	public Map<Expr, List<CExpr>> analyze(BlockSeq seq) {
		List<CExpr> inputList = new ArrayList<>();
		HashSet<OperatorExpr> opExprSet = seq.opExprColl();
		inputList.addAll(opExprSet.stream().map(CExpr::new).filter(cloneExpr -> cloneExpr.size() >= minCloneTreeSize)
				.collect(Collectors.toList()));
		return process(inputList);
	}

	public Map<Expr, List<CExpr>> process(List<CExpr> inputList) {
		groupingStrategy = CExpr::hash;
		parentLink = CExpr::getParent;
		seqPriorityFunc = CExpr::priority;
		parentPriority = (entry1, entry2) -> new CExpr(entry1.getKey()).size() - new CExpr(entry2.getKey()).size();

		Map<Integer, List<CExpr>> filteredGroups = groupAllPossibleCNodes(inputList);
		log.debug("filteredGroups:" + filteredGroups);
		Map<Expr, List<CExpr>> orderedMap = kvOrderingForOverlapElim(filteredGroups);
		log.debug("ordering for overlap elim:" + orderedMap);
		Map<Expr, List<CExpr>> resultMap = eliminateOverlapCNodes(orderedMap);

		return resultMap;
	}

	private Map<Expr, List<CExpr>> eliminateOverlapCNodes(Map<Expr, List<CExpr>> orderedByValue) {
		List<CExpr> eliminated = new ArrayList<>();
		for (Expr parentKey : orderedByValue.keySet()) {
			List<CExpr> candidates = orderedByValue.get(parentKey);
			for (int i = 0; i < candidates.size(); i++) {
				CExpr seqI = candidates.get(i);
				if (eliminated.contains(seqI)) {
					continue;
				}

				for (int j = i + 1; j < candidates.size(); j++) {
					CExpr seqJ = candidates.get(j);
					if (eliminated.contains(seqJ)) {
						continue;
					}

					Set<ASTNode> seqIElms = new HashSet<>(exprSerializer.serialize(seqI.get()));
					Set<ASTNode> seqJElms = new HashSet<>(exprSerializer.serialize(seqJ.get()));
					seqIElms.retainAll(seqJElms);
					if (!seqIElms.isEmpty()) { // if not empty then it's overlapping
						log.debug(seqI + "is overlapped with" + seqJ);
						markEliminatedCommonSeqFromMap(orderedByValue, seqJ, eliminated);
					}
				}
			}
		}

		return orderedByValue.entrySet().stream().collect(toMap(e -> e.getKey(),
				e -> e.getValue().stream().filter(seq -> !eliminated.contains(seq)).collect(Collectors.toList()),
				(oldValue, newValue) -> oldValue, LinkedHashMap::new));
	}

	private void markEliminatedCommonSeqFromMap(Map<Expr, List<CExpr>> orderedByValue, CExpr seqJ,
			List<CExpr> eliminated) {
		log.debug("remove:" + seqJ);
		eliminated.add(seqJ);
		for (List<CExpr> group : orderedByValue.values()) {
			log.debug("before" + group);
			group.forEach(seq -> {
				if (seq.equals(seqJ)) {
					eliminated.add(seq);
				}
			});
		}

	}

	private Map<Integer, List<CExpr>> groupAllPossibleCNodes(List<CExpr> inputList) {
		Map<Integer, List<CExpr>> groupByHash = inputList.stream().collect(groupingBy(groupingStrategy));
		log.debug("{}", groupByHash);

		Map<Integer, List<CExpr>> filteredGroups = groupByHash.entrySet().stream()
				.filter(e -> e.getValue().size() >= minCCGroupSize).collect(toMap(Entry::getKey, Entry::getValue));
		log.debug("{}", filteredGroups);
		return filteredGroups;
	}

	private Map<Expr, List<CExpr>> kvOrderingForOverlapElim(Map<Integer, List<CExpr>> filteredGroups) {
		Map<Expr, List<CExpr>> parentToSortedSeq = filteredGroups.values().stream().flatMap(Collection::stream)
				.collect(groupingBy(parentLink, toSortedList(seqPriorityFunc)));
		log.debug("parentToSortedSeq:" + parentToSortedSeq);

		Map<Expr, List<CExpr>> orderedByValue = parentToSortedSeq.entrySet().stream().sorted(parentPriority.reversed())
				.collect(toMap(Entry::getKey, Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		log.debug("Ordered:" + orderedByValue);
		return orderedByValue;
	}

}
