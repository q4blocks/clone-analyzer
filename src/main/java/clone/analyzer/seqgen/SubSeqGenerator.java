package clone.analyzer.seqgen;

import java.util.HashSet;
import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import ast.BlockSeq;
import ast.Program;
import ast.Script;
import ast.Scriptable;
import ast.Sprite;
import clone.analyzer.ASTCollectorFunctions;
import clone.analyzer.SequenceGenStrategy;
import clone.data.StmtSeq;

public class SubSeqGenerator<T> {
	private SequenceGenStrategy type;
	private Multimap<T, List<StmtSeq>> result = ArrayListMultimap.create();
	private int minSubseqSize = 2;

	public void setMinSubseqSize(int val) {
		minSubseqSize = val;
	}

	public SubSeqGenerator(SequenceGenStrategy type) {
		this.type = type;
	}

	/**
	 * generate all possible subsequence group by ScopeType
	 * 
	 * @param program
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Multimap<T, List<StmtSeq>> gen(Program program) {
		if (type.equals(SequenceGenStrategy.ALL_POSSIBLE_SEQ_IN_PROGRAM)) {
			ASTCollectorFunctions collector = new ASTCollectorFunctions(minSubseqSize);
			HashSet<BlockSeq> seqColl = program.blockSeqColl();
			List<StmtSeq> subseqColl = Lists.newArrayList();
			for (BlockSeq seq : seqColl) {
				subseqColl.addAll(collector.collect(seq));
			}
			if (!subseqColl.isEmpty()) {
				result.put((T) program, subseqColl);
			}
			return result;

		}
		if (type.equals(SequenceGenStrategy.GROUP_BY_SPRITE)) {
			ASTCollectorFunctions collector = new ASTCollectorFunctions(minSubseqSize);
			for (Scriptable s : program.getStageAndSprites()) {
				HashSet<BlockSeq> seqColl = s.blockSeqColl();
				List<StmtSeq> subseqColl = Lists.newArrayList();
				for (BlockSeq seq : seqColl) {
					subseqColl.addAll(collector.collect(seq));
				}
				if (!subseqColl.isEmpty()) {
					result.put((T) s, subseqColl);
				}
			}
			return result;
		}

		// NO script scope yet; need a label type for script and procedure declaration?

		if (type.equals(SequenceGenStrategy.GROUP_BY_BLOCKSEQS)) {
			ASTCollectorFunctions collector = new ASTCollectorFunctions(minSubseqSize);
			HashSet<BlockSeq> seqColl = program.blockSeqColl();
			for (BlockSeq seq : seqColl) {
				List<StmtSeq> subseqColl = collector.collect(seq);
				if (!subseqColl.isEmpty()) {
					result.put((T) seq, subseqColl);
				}
			}
			return result;
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public Multimap<T, List<StmtSeq>> gen(Scriptable s) {
		ASTCollectorFunctions collector = new ASTCollectorFunctions(minSubseqSize);

		HashSet<BlockSeq> seqColl = s.blockSeqColl();
		List<StmtSeq> subseqColl = Lists.newArrayList();
		for (BlockSeq seq : seqColl) {
			subseqColl.addAll(collector.collect(seq));
		}
		if (!subseqColl.isEmpty()) {
			result.put((T) s, subseqColl);
		}
		return result;
	}

}
