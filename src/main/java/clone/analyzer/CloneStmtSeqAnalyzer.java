package clone.analyzer;

import static clone.analyzer.CustomCollectors.toSortedList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import ast.ASTNode;
import ast.BlockSeq;
import ast.Program;
import ast.Scriptable;
import ast.Sprite;
import ast.Stmt;
import clone.analyzer.seqgen.SubSeqGenerator;
import clone.data.StmtSeq;
import clone.result.CloneGroup;
import clone.result.CloneSeqResult;

public class CloneStmtSeqAnalyzer<T> extends AbstractCloneStmtSeqAnalyzer {

	private static final Logger logger = LogManager.getLogger("CloneStmtSeqAnalyzer");
	int minCCGroupSize = 2;
	private int minSubseqSize = 2;

	private Function<StmtSeq, Integer> groupingStrategy;
	private Function<StmtSeq, BlockSeq> parentLink;
	private Comparator<StmtSeq> seqPriorityFunc;
	private Comparator<Entry<BlockSeq, List<StmtSeq>>> parentPriority;
	private SubSeqGenerator<T> seqGenerator;

	public CloneStmtSeqAnalyzer(SequenceGenStrategy type) {
		Configurator.setLevel("CloneStmtSeqAnalyzer", Level.ERROR);
		this.seqGenerator = new SubSeqGenerator<>(type);
		this.seqGenerator.setMinSubseqSize(minSubseqSize);
	}

	public void setMinSubseqSize(int val) {
		this.minSubseqSize = val;
		this.seqGenerator.setMinSubseqSize(minSubseqSize);
	}

	public void setGroupSize(int size) {
		this.minCCGroupSize = size;
	}

	/**
	 * analyze and collect clone groups within the specified scope
	 * 
	 * @param program
	 * @return
	 */
	public CloneSeqResult<T> analyze(Program program) {
		Multimap<T, List<StmtSeq>> results = ArrayListMultimap.create();
		Multimap<T, List<StmtSeq>> subseqMap = seqGenerator.gen(program);
		for (Entry<T, List<StmtSeq>> entry : subseqMap.entries()) {
			Map<BlockSeq, List<StmtSeq>> blockSeqToCloneSeqs = process(entry.getValue());
			List<StmtSeq> resultForEntry = blockSeqToCloneSeqs.values().stream().flatMap(Collection::stream)
					.collect(Collectors.toList());
			if (!resultForEntry.isEmpty()) {
				results.put(entry.getKey(), resultForEntry);
			}
		}
		return new CloneSeqResult<T>().of(results);
	}

	public Map<BlockSeq, List<StmtSeq>> analyzePerBlockSeq(Program program) {
		ASTCollectorFunctions collector = new ASTCollectorFunctions();
		Map<BlockSeq, List<StmtSeq>> results = new HashMap<>();

		for (Scriptable sprite : program.getStageAndSprites()) {
			for (BlockSeq seq : sprite.blockSeqColl()) {
				List<StmtSeq> stmtSeqList = new ArrayList<>();
				stmtSeqList.addAll(collector.collect(seq));
				if (stmtSeqList.isEmpty()) {
					continue;
				}
				Map<BlockSeq, List<StmtSeq>> res = process(stmtSeqList);
				results.putAll(res);
			}
		}

		return results;
	}

	// only analyze sprite together no stage
	public CloneSeqResult<T> analyze(Sprite sprite) {
		Multimap<T, List<StmtSeq>> results = ArrayListMultimap.create();
		Multimap<T, List<StmtSeq>> subseqMap = seqGenerator.gen(sprite);
		for (Entry<T, List<StmtSeq>> entry : subseqMap.entries()) {
			Map<BlockSeq, List<StmtSeq>> blockSeqToCloneSeqs = process(entry.getValue());
			List<StmtSeq> resultForEntry = blockSeqToCloneSeqs.values().stream().flatMap(Collection::stream)
					.collect(Collectors.toList());
			if (!resultForEntry.isEmpty()) {
				results.put(entry.getKey(), resultForEntry);
			}
		}
		return new CloneSeqResult<T>().of(results);

	}

	public Map<BlockSeq, List<StmtSeq>> process(java.util.List<StmtSeq> inputList) {
		groupingStrategy = StmtSeq::hash;
		parentLink = StmtSeq::getParent;
		seqPriorityFunc = StmtSeq::priority;
		parentPriority = (entry1, entry2) -> entry1.getKey().getNumStmt() - entry2.getKey().getNumStmt();

		/**
		 * quick fix to disregard conditional parent block types: 
		 * to allow its children to become candidate clone
		 * and avoid the case where new variable needs to be declare 
		 * to hold boolean value and equality check with "true" string in the conditional expr
		 */
		
		List<StmtSeq> inputList2 = inputList.stream()
				.filter(seq -> {

					Set<String> conditionalTypes = Sets.newHashSet("control_repeat_until","control_if","control_if_else");
					if(conditionalTypes.contains(seq.get().get(0).getOpcode()))
						return false;
					return true;
			}).collect(Collectors.toList());
		Map<Integer, List<StmtSeq>> filteredGroups = groupAllPossibleCNodes(inputList2);		
		logger.info("filteredGroups:" + filteredGroups);
		Map<BlockSeq, List<StmtSeq>> orderedMap = kvOrderingForOverlapElim(filteredGroups);
		logger.info("ordering for overlap elim:" + orderedMap);
		Map<BlockSeq, List<StmtSeq>> resultMap = removeOverlapInSameBlockSeq(orderedMap);
		// TODO: removeOverlapInSameRootSeq
		Map<BlockSeq, List<StmtSeq>> resultMap2 = rootSeqToItsCandidateSeqs(resultMap);
		Map<BlockSeq, List<StmtSeq>> resultMap3 = removeOverlapInSameBlockSeq(resultMap2);
		return resultMap3;
	}

	private Map<BlockSeq, List<StmtSeq>> rootSeqToItsCandidateSeqs(Map<BlockSeq, List<StmtSeq>> resultMap) {
		Function<StmtSeq, BlockSeq> rootLink = seq -> seq.get().get(0).rootSeq();
		Comparator<StmtSeq> seqTreeSizeComp = (seq1, seq2) -> {
			int seqPriority = serializeStmts(seq1.get()).size() - serializeStmts(seq2.get()).size();
			if (seqPriority == 0) {
				seqPriority = 1; // choose seq1
			}
			return seqPriority;
		};
		Comparator<Entry<BlockSeq, List<StmtSeq>>> rootSeqPriority = (entry1, entry2) -> {
			return entry1.getKey().stmtColl().size() - entry2.getKey().stmtColl().size();
		};

		Map<BlockSeq, List<StmtSeq>> rootSeqToSortedSeq = resultMap.values().stream().flatMap(Collection::stream)
				.collect(groupingBy(rootLink, toSortedList(seqTreeSizeComp)));
		logger.info("rootSeqToSortedSeq:" + rootSeqToSortedSeq);

		Map<BlockSeq, List<StmtSeq>> orderedByValue = rootSeqToSortedSeq.entrySet().stream()
				.sorted(rootSeqPriority.reversed())
				.collect(toMap(Entry::getKey, Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		logger.info("Ordered:" + orderedByValue);
		return orderedByValue;
	}

	private Map<BlockSeq, List<StmtSeq>> removeOverlapInSameBlockSeq(Map<BlockSeq, List<StmtSeq>> orderedByValue) {
		List<StmtSeq> eliminated = new ArrayList<>();
		for (BlockSeq parentKey : orderedByValue.keySet()) {
			List<StmtSeq> candidates = orderedByValue.get(parentKey);
			for (int i = 0; i < candidates.size(); i++) {
				StmtSeq seqI = candidates.get(i);
				if (eliminated.contains(seqI)) {
					continue;
				}

				for (int j = i + 1; j < candidates.size(); j++) {
					StmtSeq seqJ = candidates.get(j);
					if (eliminated.contains(seqJ)) {
						continue;
					}
					Set<Stmt> seqIElms = serializeStmts(seqI.get());
					Set<Stmt> seqJElms = serializeStmts(seqJ.get());
					seqIElms.retainAll(seqJElms); // intersection
					if (!seqIElms.isEmpty()) { // if not empty then it's overlapping
						logger.info(seqI + "is overlapped with" + seqJ);
						markEliminatedCommonSeqFromMap(orderedByValue, seqJ, eliminated);
					}
				}
			}
		}

		Map<BlockSeq, List<StmtSeq>> result = orderedByValue.entrySet().stream().collect(toMap(e -> e.getKey(),
				e -> e.getValue().stream().filter(seq -> !eliminated.contains(seq)).collect(Collectors.toList()),
				(oldValue, newValue) -> oldValue, LinkedHashMap::new));

		return result;
	}

	private void markEliminatedCommonSeqFromMap(Map<BlockSeq, List<StmtSeq>> orderedByValue, StmtSeq seqJ,
			List<StmtSeq> eliminated) {
		logger.info("remove:" + seqJ);
		eliminated.add(seqJ);
		for (List<StmtSeq> group : orderedByValue.values()) {
			logger.info("before" + group);
			group.forEach(seq -> {
				if (seq.equals(seqJ)) {
					eliminated.add(seq);
				}
			});
		}

	}

	private Map<BlockSeq, List<StmtSeq>> kvOrderingForOverlapElim(Map<Integer, List<StmtSeq>> filteredGroups) {
		Map<BlockSeq, List<StmtSeq>> parentToSortedSeq = filteredGroups.values().stream().flatMap(Collection::stream)
				.collect(groupingBy(parentLink, toSortedList(seqPriorityFunc)));
		logger.info("parentToSortedSeq:" + parentToSortedSeq);

		Map<BlockSeq, List<StmtSeq>> orderedByValue = parentToSortedSeq.entrySet().stream()
				.sorted(parentPriority.reversed())
				.collect(toMap(Entry::getKey, Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		logger.info("Ordered:" + orderedByValue);
		return orderedByValue;
	}

	private Map<Integer, List<StmtSeq>> groupAllPossibleCNodes(List<StmtSeq> inputList) {
		Map<Integer, List<StmtSeq>> groupByHash = inputList.stream().collect(groupingBy(groupingStrategy));
		logger.debug(groupByHash);

		Map<Integer, List<StmtSeq>> filteredGroups = groupByHash.entrySet().stream().filter(e -> {
			boolean isSufficientSize = e.getValue().size() >= minCCGroupSize;
			return isSufficientSize;

		}).collect(toMap(Entry::getKey, Entry::getValue));
		logger.debug(filteredGroups);
		return filteredGroups;
	}

	static Set<Stmt> serializeStmts(List<Stmt> subseq) {
		SerializationConfig config = new SerializationConfig.Builder().skipField(true).skipInput(true).build();
		List<ASTNode> serialized = new ASTNodeSerializer(config).serializeASTSeq(subseq); // TODO: BROKEN SHOULD SKIP
																							// STILL OUTPUT INPUT arg

		return new HashSet<Stmt>(
				serialized.stream().filter(n -> n instanceof Stmt).map(n -> (Stmt) n).collect(Collectors.toList()));
	}

}
