package clone.analyzer;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import ast.Sprite;
import ast.Stmt;
import clone.data.StmtSeq;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Hash Value wrapper for Sprite based on scripts contained in the sprite
 * (excluding unreachable scripts)
 * Hash based on statement blocks and nonparameterizable inputs
 * 
 * @author karn
 *
 */

@Slf4j
public class SpriteHash {
	
	@Getter
	private Sprite sprite;
	@Getter
	Set<StmtSeq> stmtSeqSet = Sets.newHashSet();

	public SpriteHash(Sprite sprite) {
		this.sprite = sprite;
		sprite.getScriptList().forEach(s -> {
			// only if script starts with event block
			try {
				if (s != null && s.getBody().getNumStmt() > 0 && s.getBody().getStmt(0).isEventBlock()) {
					List<Stmt> stmtList = Lists.newArrayList(s.getBody().getStmtList());
					StmtSeq stmtSeq = new StmtSeq(stmtList, s.getBody());
					stmtSeqSet.add(stmtSeq);
				}
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		});
	}

	public int hash() {
		Set<Integer> hashcodes = this.stmtSeqSet.stream().map(s -> s.hash())
				.collect(Collectors.toSet());

		return hashcodes.stream().reduce(0, (left, right) -> left ^ right);
	}

	@Override
	public String toString() {
		return "SpriteSeqSet [" + sprite.getName() + "]";
	}

}
