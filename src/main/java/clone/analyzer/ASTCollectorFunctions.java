package clone.analyzer;

import static clone.utils.SlidingWindowFunctions.slidingWindowOfAll;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import ast.BlockSeq;
import ast.ScratchBlock;
import ast.Stmt;
import clone.data.StmtSeq;

public class ASTCollectorFunctions {

	private int minSubseqSize = 2;

	public ASTCollectorFunctions(int minSubseqSize) {
		this.minSubseqSize = minSubseqSize;
	}
	
	public ASTCollectorFunctions() {
		this(2);
	}

	private List<List<Stmt>> collect(Iterable<Stmt> stmtIterable) {
		ArrayList<Stmt> list = Lists.newArrayList(stmtIterable);
		// TODO: add skip first block of certain type as configuration
		ScratchBlock blk = (ScratchBlock) list.get(0);
		if (blk.getOpcode().contains("event_when") || blk.getOpcode().contains("control_start_as_clone")) {
			list.remove(0);
		}

		List<List<Stmt>> subseqColl = slidingWindowOfAll(list, subseq -> {
			if (subseq.size() >= minSubseqSize) {
				return true; // consider forest of at least 2 trees (for simple stmt)
			} else {
				return subseq.get(0).hasNestedSeq(); // subseq that contains blockSeq should be considered
			}
		});
		return subseqColl;
	}

	public List<StmtSeq> collect(BlockSeq seq) {
		List<List<Stmt>> list = collect(seq.getStmtList());
		return list.stream().map(stmtList -> new StmtSeq(stmtList, seq)).collect(Collectors.toList());
	}

}
