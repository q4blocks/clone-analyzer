package clone.analyzer;

public enum SequenceGenStrategy {
	GROUP_BY_BLOCKSEQS, PER_SCRIPT, GROUP_BY_SPRITE, ALL_POSSIBLE_SEQ_IN_PROGRAM
}
