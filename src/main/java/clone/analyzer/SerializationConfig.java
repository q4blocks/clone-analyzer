package clone.analyzer;

import java.awt.List;
import java.util.ArrayList;

import com.google.common.collect.Lists;

import ast.ASTNode;
import ast.AttrAccess;
import ast.Expr;
import ast.NumLiteral;
import ast.VarAccess;
import lombok.extern.slf4j.Slf4j;

/**
 * AST node serialization logic for Scratch3 ASTNode
 * 
 * @author karn
 *
 */
@Slf4j
public class SerializationConfig {
	private final boolean skipField;
	private final boolean skipInput;
	private final boolean skipChildExpr;

	public boolean shouldSkip(ASTNode node) {
		ArrayList<Class<? extends ASTNode>> skippables = Lists.newArrayList(ast.AttrAccess.class);
		boolean skip = false;
		if (node instanceof Expr) {
			if (skipInput && ((Expr) node).isRootExpr()) {
				skip = true;
				if(node instanceof VarAccess && ((VarAccess) node).isDest()) {					
					skip = false;
				}
			}

			if (!skipInput && skipChildExpr && !((Expr) node).isRootExpr()) {
				skip = true;
			}
		}
		for (Class<? extends ASTNode> klass : skippables) {
			if (klass.isInstance(node) && skipField) {
				skip = true;
			}
		}
		return skip;
	}

	public boolean skipField() {
		return skipField;
	}

	public boolean skipInput() {
		return skipInput;
	}

	public boolean skipChildExpr() {
		return skipChildExpr;
	}

	public boolean isAtomic(ASTNode node) {
		if (node instanceof AttrAccess) {
			return true;
		}
		return false;
	}

	public static class Builder {
		private boolean skipField = true;
		private boolean skipInput = false;
		private boolean skipChildExpr = false;

		public Builder skipField(boolean val) {
			skipField = val;
			return this;
		}

		public Builder skipInput(boolean val) {
			skipInput = val;
			return this;
		}

		public Builder skipChildExpr(boolean val) {
			skipChildExpr = val;
			return this;
		}

		public SerializationConfig build() {
			return new SerializationConfig(this);
		}
	}

	public static SerializationConfig defaultConfig() {
		return new SerializationConfig.Builder().skipField(false).build();
	}

	private SerializationConfig(Builder builder) {
		skipField = builder.skipField;
		skipInput = builder.skipInput;
		skipChildExpr = builder.skipChildExpr;
	}
}
