package clone.analyzer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class CustomCollectors {
	public static <T, A, R> Collector<T, ?, R> filtering(Predicate<? super T> predicate,
			Collector<? super T, A, R> downstream) {

		BiConsumer<A, ? super T> accumulator = downstream.accumulator();
		return Collector.of(downstream.supplier(), (r, t) -> {
			if (predicate.test(t))
				accumulator.accept(r, t);
		}, downstream.combiner(), downstream.finisher(),
				downstream.characteristics().toArray(new Collector.Characteristics[0]));
	}
	
	public static <T> Collector<T,?,List<T>> toSortedList(Comparator<? super T> c) {
	    return Collectors.collectingAndThen(
	        Collectors.toCollection(ArrayList::new), l->{ l.sort(c.reversed()); return l; } );
	}
	
}
