package clone.analyzer;

import java.util.Iterator;
import java.util.List;

import com.google.common.collect.Lists;

import ast.ASTNode;
import ast.AttrAccess;
import ast.Expr;
import ast.Literal;
import ast.ScratchBlock;
import ast.VarAccess;

/**
 * ASTNodeHasher should be designed to have no knowledge of the input
 * (serialized nodes) or a leaf node It will only consider the top-level of each
 * node of the input list ASTNodeHasher accepts list of ASTNode and produce hash
 * based on top level ast node
 * 
 * @author karn
 *
 */

public class ASTNodeHasher {
	private boolean distinguishVarRead;

	public ASTNodeHasher(boolean shouldDistinguishVarRead) {
		// Expr Clone Analysis as well as Parameter Analysis should set to true so
		// expression with different var should be considered different
		// Seq of Stmt clone analysis should set to false, so as to allow
		// parameterization of different expressions to command block (in this case
		// different variables)
		this.distinguishVarRead = shouldDistinguishVarRead;
	}

	public int hashOf(ASTNode leafNode) {
		return hashOf(Lists.newArrayList(leafNode));
	}

	public int hashOf(List<ASTNode> serializedAstNodes) {
		int hashCode = 1;
		Iterator<ASTNode> i = serializedAstNodes.iterator();
		while (i.hasNext()) {
			Object obj = i.next();
			int typeHash = 0;
			if (obj instanceof VarAccess && (!((VarAccess) obj).isReplacable() || distinguishVarRead)) {
				String typeVarName = ((VarAccess) obj).getType() + ((VarAccess) obj).getName().hashCode() * 31;
				typeHash = typeVarName.hashCode();
			} else if (obj instanceof ScratchBlock) {// this includes VarAccess has isReplacable = true;
				typeHash = ((ScratchBlock) obj).getOpcode().hashCode();
			} else if (obj instanceof Literal) {
				String typeValueComb = obj.getClass().getSimpleName()
						+ (((Literal) obj).getValue() == null ? "" : ((Literal) obj).getValue().hashCode() * 31);
				typeHash = typeValueComb.hashCode();
			} else if (obj instanceof AttrAccess) {
				String typeValueComb = obj.getClass().getSimpleName()
						+ (((AttrAccess) obj).getID() == null ? "" : ((AttrAccess) obj).getID());
				typeHash = typeValueComb.hashCode();
			}
			hashCode = 31 * hashCode + (obj == null ? 0 : typeHash);
		}
		return hashCode;
	}

	public int hashOfExpr(Expr expr) {
		SerializationConfig config = new SerializationConfig.Builder().skipField(false).skipInput(false).build();
		List<ASTNode> serialized = new ASTNodeSerializer(config).serialize(expr);
		return hashOf(serialized);
	}
}
