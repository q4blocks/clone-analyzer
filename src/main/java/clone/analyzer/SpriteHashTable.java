package clone.analyzer;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;

import ast.Sprite;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SpriteHashTable {
	ListMultimap<Integer, SpriteHash> table = ArrayListMultimap.create();

	public void addSprite(Sprite sprite) {
		SpriteHash spriteSeqSet = new SpriteHash(sprite);
		table.put(spriteSeqSet.hash(), spriteSeqSet);
	}

	public ListMultimap<Integer, SpriteHash> get() {
		return table;
	}

	public static SpriteHashTable of(Collection<Sprite> sprites) {
		SpriteHashTable fragmentTable = new SpriteHashTable();
		/*
		 * ignore empty sprite (any empty sprites have the same hash and
		 * should be discarded)
		 */

		sprites.stream().filter(s -> s.getNumScript() > 0).forEach(s -> fragmentTable.addSprite(s));
		fragmentTable.removeSingleton();
		fragmentTable.removeZeroHash();
		return fragmentTable;
	}

	/**
	 * remove sprite group that has no event script
	 */
	private void removeZeroHash() {
		if (this.table.keys().contains(0)) {
			this.table.removeAll(0);
		}
	}

	private void removeSingleton() {
		List<Integer> keysToRemove = Lists.newArrayList();
		for (Integer hashKey : this.table.keys()) {
			if (table.get(hashKey).size() == 1) {
				keysToRemove.add(hashKey);
			}
		}

		keysToRemove.forEach(k -> table.removeAll(k));

	}

}
